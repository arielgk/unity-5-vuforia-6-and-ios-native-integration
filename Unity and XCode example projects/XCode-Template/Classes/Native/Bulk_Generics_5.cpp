﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// System.Object
struct Il2CppObject;
// System.String
struct String_t;
// System.Collections.Generic.LinkedList`1<System.Object>
struct LinkedList_1_t2045451540;
// System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManagerImpl/IdPair>
struct LinkedList_1_t1397221934;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t2185721892;
// System.Array
struct Il2CppArray;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// System.Collections.Generic.LinkedListNode`1<System.Object>
struct LinkedListNode_1_t3787551078;
// System.Object[]
struct ObjectU5BU5D_t1108656482;
// System.Collections.Generic.IEnumerator`1<Vuforia.VuforiaManagerImpl/IdPair>
struct IEnumerator_1_t1139484518;
// System.Collections.Generic.LinkedListNode`1<Vuforia.VuforiaManagerImpl/IdPair>
struct LinkedListNode_1_t3139321472;
// Vuforia.VuforiaManagerImpl/IdPair[]
struct IdPairU5BU5D_t296135328;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t2522024052;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t1244034627;
// System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>
struct List_1_t132831245;
// System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>
struct List_1_t374511678;
// System.Collections.Generic.List`1<Vuforia.CameraDevice/CameraField>
struct List_1_t4242749885;
// System.Collections.Generic.List`1<Vuforia.Image/PIXEL_FORMAT>
struct List_1_t1722560608;
// System.Collections.Generic.List`1<Vuforia.TargetFinder/TargetSearchResult>
struct List_1_t682628370;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066860316.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24066860316MethodDeclarations.h"
#include "mscorlib_System_Object4170816371.h"
#include "mscorlib_System_Void2863195528.h"
#include "mscorlib_System_Int321153838500.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_Int321153838500MethodDeclarations.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_731195302.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_731195302MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_TrackableBehaviour_835151357.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_355424280.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_355424280MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl_459380335.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22545618620.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22545618620MethodDeclarations.h"
#include "mscorlib_System_Boolean476798718.h"
#include "mscorlib_System_Boolean476798718MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23222658402.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_23222658402MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21944668977MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22093487825.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22093487825MethodDeclarations.h"
#include "mscorlib_System_UInt1624667923.h"
#include "mscorlib_System_UInt1624667923MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24030510692.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_24030510692MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_WebCamProfile_Prof1961690790.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21718082560.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_21718082560MethodDeclarations.h"
#include "Vuforia_UnityExtensions_Vuforia_Image_PIXEL_FORMAT354375056.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu3528606100.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu3528606100MethodDeclarations.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2045451540.h"
#include "System_System_Collections_Generic_LinkedListNode_13787551078.h"
#include "mscorlib_System_UInt3224667981.h"
#include "mscorlib_System_ObjectDisposedException1794727681MethodDeclarations.h"
#include "mscorlib_System_InvalidOperationException1589641621MethodDeclarations.h"
#include "mscorlib_System_ObjectDisposedException1794727681.h"
#include "mscorlib_System_InvalidOperationException1589641621.h"
#include "System_System_Collections_Generic_LinkedListNode_13787551078MethodDeclarations.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu2880376494.h"
#include "System_System_Collections_Generic_LinkedList_1_Enu2880376494MethodDeclarations.h"
#include "System_System_Collections_Generic_LinkedList_1_gen1397221934.h"
#include "System_System_Collections_Generic_LinkedListNode_13139321472.h"
#include "Vuforia_UnityExtensions_Vuforia_VuforiaManagerImpl3522586765.h"
#include "System_System_Collections_Generic_LinkedListNode_13139321472MethodDeclarations.h"
#include "System_System_Collections_Generic_LinkedList_1_gen2045451540MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892.h"
#include "mscorlib_System_Runtime_Serialization_StreamingCon2761351129.h"
#include "mscorlib_System_ArgumentException928607144MethodDeclarations.h"
#include "mscorlib_System_ArgumentException928607144.h"
#include "mscorlib_System_ArgumentNullException3573189601MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException3573189601.h"
#include "mscorlib_System_Array1146569071MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3816648464MethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException3816648464.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "mscorlib_System_Runtime_Serialization_Serializatio2185721892MethodDeclarations.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_RuntimeTypeHandle2669177232.h"
#include "System_System_Collections_Generic_LinkedList_1_gen1397221934MethodDeclarations.h"
#include "Vuforia.UnityExtensions_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2541696822.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera2541696822MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2522024052.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1263707397MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1244034627.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat152504015.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat152504015MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen132831245.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArg3059612989.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat394184448.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat394184448MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen374511678.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArg3301293422.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4262422655.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera4262422655MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen4242749885.h"
#include "Vuforia_UnityExtensions_Vuforia_CameraDevice_Camer2874564333.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1742233378.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumera1742233378MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1722560608.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat702301140.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerat702301140MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen682628370.h"
#include "Vuforia_UnityExtensions_Vuforia_TargetFinder_Targe3609410114.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m11197230_gshared (KeyValuePair_2_t4066860316 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		((  void (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4066860316 *)__this), (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4066860316 *)__this), (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m11197230_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t4066860316 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4066860316 *>(__this + 1);
	KeyValuePair_2__ctor_m11197230(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m494458106_gshared (KeyValuePair_2_t4066860316 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m494458106_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4066860316 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4066860316 *>(__this + 1);
	return KeyValuePair_2_get_Key_m494458106(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m4229413435_gshared (KeyValuePair_2_t4066860316 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m4229413435_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t4066860316 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4066860316 *>(__this + 1);
	KeyValuePair_2_set_Key_m4229413435(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1563175098_gshared (KeyValuePair_2_t4066860316 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1563175098_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4066860316 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4066860316 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1563175098(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1296398523_gshared (KeyValuePair_2_t4066860316 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1296398523_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t4066860316 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4066860316 *>(__this + 1);
	KeyValuePair_2_set_Value_m1296398523(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m491888647_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m491888647_gshared (KeyValuePair_2_t4066860316 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m491888647_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4066860316 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4066860316 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m1286526384((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4066860316 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4066860316 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m491888647_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4066860316 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4066860316 *>(__this + 1);
	return KeyValuePair_2_ToString_m491888647(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3819392495_gshared (KeyValuePair_2_t731195302 * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		((  void (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t731195302 *)__this), (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		((  void (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t731195302 *)__this), (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3819392495_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t731195302 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t731195302 *>(__this + 1);
	KeyValuePair_2__ctor_m3819392495(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m1648260377_gshared (KeyValuePair_2_t731195302 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m1648260377_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t731195302 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t731195302 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1648260377(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2950484698_gshared (KeyValuePair_2_t731195302 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2950484698_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t731195302 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t731195302 *>(__this + 1);
	KeyValuePair_2_set_Key_m2950484698(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m2058197529_gshared (KeyValuePair_2_t731195302 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m2058197529_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t731195302 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t731195302 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2058197529(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m4051642842_gshared (KeyValuePair_2_t731195302 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m4051642842_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t731195302 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t731195302 *>(__this + 1);
	KeyValuePair_2_set_Value_m4051642842(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.TrackableBehaviour/Status>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m3895345992_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m3895345992_gshared (KeyValuePair_2_t731195302 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3895345992_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t731195302 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t731195302 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m1286526384((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		int32_t L_8 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t731195302 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t731195302 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3895345992_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t731195302 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t731195302 *>(__this + 1);
	return KeyValuePair_2_ToString_m3895345992(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2676204643_gshared (KeyValuePair_2_t355424280 * __this, int32_t ___key0, VirtualButtonData_t459380335  ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		((  void (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t355424280 *)__this), (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		VirtualButtonData_t459380335  L_1 = ___value1;
		((  void (*) (Il2CppObject *, VirtualButtonData_t459380335 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t355424280 *)__this), (VirtualButtonData_t459380335 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2676204643_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, VirtualButtonData_t459380335  ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t355424280 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t355424280 *>(__this + 1);
	KeyValuePair_2__ctor_m2676204643(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m4182360997_gshared (KeyValuePair_2_t355424280 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m4182360997_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t355424280 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t355424280 *>(__this + 1);
	return KeyValuePair_2_get_Key_m4182360997(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2442740710_gshared (KeyValuePair_2_t355424280 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2442740710_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t355424280 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t355424280 *>(__this + 1);
	KeyValuePair_2_set_Key_m2442740710(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::get_Value()
extern "C"  VirtualButtonData_t459380335  KeyValuePair_2_get_Value_m1627425673_gshared (KeyValuePair_2_t355424280 * __this, const MethodInfo* method)
{
	{
		VirtualButtonData_t459380335  L_0 = (VirtualButtonData_t459380335 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  VirtualButtonData_t459380335  KeyValuePair_2_get_Value_m1627425673_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t355424280 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t355424280 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1627425673(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3460576486_gshared (KeyValuePair_2_t355424280 * __this, VirtualButtonData_t459380335  ___value0, const MethodInfo* method)
{
	{
		VirtualButtonData_t459380335  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3460576486_AdjustorThunk (Il2CppObject * __this, VirtualButtonData_t459380335  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t355424280 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t355424280 *>(__this + 1);
	KeyValuePair_2_set_Value_m3460576486(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,Vuforia.VuforiaManagerImpl/VirtualButtonData>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m3554271650_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m3554271650_gshared (KeyValuePair_2_t355424280 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3554271650_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	VirtualButtonData_t459380335  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t355424280 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t355424280 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		String_t* L_4 = Int32_ToString_m1286526384((int32_t*)(&V_0), /*hidden argument*/NULL);
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		VirtualButtonData_t459380335  L_8 = ((  VirtualButtonData_t459380335  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t355424280 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		VirtualButtonData_t459380335  L_9 = ((  VirtualButtonData_t459380335  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t355424280 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (VirtualButtonData_t459380335 )L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3554271650_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t355424280 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t355424280 *>(__this + 1);
	return KeyValuePair_2_ToString_m3554271650(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2040323320_gshared (KeyValuePair_2_t2545618620 * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2545618620 *)__this), (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		bool L_1 = ___value1;
		((  void (*) (Il2CppObject *, bool, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2545618620 *)__this), (bool)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2040323320_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, bool ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t2545618620 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2545618620 *>(__this + 1);
	KeyValuePair_2__ctor_m2040323320(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m700889072_gshared (KeyValuePair_2_t2545618620 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m700889072_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2545618620 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2545618620 *>(__this + 1);
	return KeyValuePair_2_get_Key_m700889072(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1751794225_gshared (KeyValuePair_2_t2545618620 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1751794225_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2545618620 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2545618620 *>(__this + 1);
	KeyValuePair_2_set_Key_m1751794225(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::get_Value()
extern "C"  bool KeyValuePair_2_get_Value_m3809014448_gshared (KeyValuePair_2_t2545618620 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (bool)__this->get_value_1();
		return L_0;
	}
}
extern "C"  bool KeyValuePair_2_get_Value_m3809014448_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2545618620 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2545618620 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3809014448(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3162969521_gshared (KeyValuePair_2_t2545618620 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3162969521_AdjustorThunk (Il2CppObject * __this, bool ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2545618620 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2545618620 *>(__this + 1);
	KeyValuePair_2_set_Value_m3162969521(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m3396952209_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m3396952209_gshared (KeyValuePair_2_t2545618620 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m3396952209_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	bool V_1 = false;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2545618620 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2545618620 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		bool L_8 = ((  bool (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2545618620 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		bool L_9 = ((  bool (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2545618620 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (bool)L_9;
		String_t* L_10 = Boolean_ToString_m2512358154((bool*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m3396952209_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2545618620 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2545618620 *>(__this + 1);
	return KeyValuePair_2_ToString_m3396952209(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2730552978_gshared (KeyValuePair_2_t3222658402 * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3222658402 *)__this), (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_1 = ___value1;
		((  void (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3222658402 *)__this), (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2730552978_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, int32_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t3222658402 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3222658402 *>(__this + 1);
	KeyValuePair_2__ctor_m2730552978(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m4285571350_gshared (KeyValuePair_2_t3222658402 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m4285571350_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3222658402 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3222658402 *>(__this + 1);
	return KeyValuePair_2_get_Key_m4285571350(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1188304983_gshared (KeyValuePair_2_t3222658402 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1188304983_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3222658402 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3222658402 *>(__this + 1);
	KeyValuePair_2_set_Key_m1188304983(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
extern "C"  int32_t KeyValuePair_2_get_Value_m2690735574_gshared (KeyValuePair_2_t3222658402 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Value_m2690735574_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3222658402 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3222658402 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2690735574(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m137193687_gshared (KeyValuePair_2_t3222658402 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m137193687_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t3222658402 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3222658402 *>(__this + 1);
	KeyValuePair_2_set_Value_m137193687(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m2052282219_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2052282219_gshared (KeyValuePair_2_t3222658402 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2052282219_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	int32_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3222658402 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3222658402 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		int32_t L_8 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3222658402 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		int32_t L_9 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t3222658402 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (int32_t)L_9;
		String_t* L_10 = Int32_ToString_m1286526384((int32_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2052282219_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t3222658402 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t3222658402 *>(__this + 1);
	return KeyValuePair_2_ToString_m2052282219(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m4168265535_gshared (KeyValuePair_2_t1944668977 * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1944668977 *)__this), (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1944668977 *)__this), (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m4168265535_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1944668977 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1944668977 *>(__this + 1);
	KeyValuePair_2__ctor_m4168265535(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3256475977_gshared (KeyValuePair_2_t1944668977 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m3256475977_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1944668977 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1944668977 *>(__this + 1);
	return KeyValuePair_2_get_Key_m3256475977(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m1278074762_gshared (KeyValuePair_2_t1944668977 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m1278074762_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1944668977 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1944668977 *>(__this + 1);
	KeyValuePair_2_set_Key_m1278074762(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3899079597_gshared (KeyValuePair_2_t1944668977 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m3899079597_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1944668977 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1944668977 *>(__this + 1);
	return KeyValuePair_2_get_Value_m3899079597(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m2954518154_gshared (KeyValuePair_2_t1944668977 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m2954518154_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1944668977 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1944668977 *>(__this + 1);
	KeyValuePair_2_set_Value_m2954518154(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m1313859518_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m1313859518_gshared (KeyValuePair_2_t1944668977 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m1313859518_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1944668977 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1944668977 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		Il2CppObject * L_8 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1944668977 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
		if (!L_8)
		{
			G_B5_0 = 3;
			G_B5_1 = L_7;
			G_B5_2 = L_7;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1944668977 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_9;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m1313859518_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1944668977 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1944668977 *>(__this + 1);
	return KeyValuePair_2_ToString_m1313859518(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m3840239519_gshared (KeyValuePair_2_t2093487825 * __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2093487825 *)__this), (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		uint16_t L_1 = ___value1;
		((  void (*) (Il2CppObject *, uint16_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2093487825 *)__this), (uint16_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m3840239519_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, uint16_t ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t2093487825 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2093487825 *>(__this + 1);
	KeyValuePair_2__ctor_m3840239519(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1711553769_gshared (KeyValuePair_2_t2093487825 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m1711553769_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2093487825 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2093487825 *>(__this + 1);
	return KeyValuePair_2_get_Key_m1711553769(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m4131482410_gshared (KeyValuePair_2_t2093487825 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m4131482410_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2093487825 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2093487825 *>(__this + 1);
	KeyValuePair_2_set_Key_m4131482410(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::get_Value()
extern "C"  uint16_t KeyValuePair_2_get_Value_m992554829_gshared (KeyValuePair_2_t2093487825 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (uint16_t)__this->get_value_1();
		return L_0;
	}
}
extern "C"  uint16_t KeyValuePair_2_get_Value_m992554829_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2093487825 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2093487825 *>(__this + 1);
	return KeyValuePair_2_get_Value_m992554829(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3359578666_gshared (KeyValuePair_2_t2093487825 * __this, uint16_t ___value0, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3359578666_AdjustorThunk (Il2CppObject * __this, uint16_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t2093487825 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2093487825 *>(__this + 1);
	KeyValuePair_2_set_Value_m3359578666(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.UInt16>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m665911326_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m665911326_gshared (KeyValuePair_2_t2093487825 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m665911326_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	uint16_t V_1 = 0;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2093487825 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2093487825 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		uint16_t L_8 = ((  uint16_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2093487825 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		uint16_t L_9 = ((  uint16_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t2093487825 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (uint16_t)L_9;
		String_t* L_10 = UInt16_ToString_m741885559((uint16_t*)(&V_1), /*hidden argument*/NULL);
		G_B6_0 = L_10;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_11 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_11;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_12 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 4);
		ArrayElementTypeCheck (L_12, _stringLiteral93);
		(L_12)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_12, /*hidden argument*/NULL);
		return L_13;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m665911326_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t2093487825 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t2093487825 *>(__this + 1);
	return KeyValuePair_2_ToString_m665911326(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m2890662519_gshared (KeyValuePair_2_t4030510692 * __this, Il2CppObject * ___key0, ProfileData_t1961690790  ___value1, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___key0;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4030510692 *)__this), (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		ProfileData_t1961690790  L_1 = ___value1;
		((  void (*) (Il2CppObject *, ProfileData_t1961690790 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4030510692 *)__this), (ProfileData_t1961690790 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m2890662519_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___key0, ProfileData_t1961690790  ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t4030510692 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4030510692 *>(__this + 1);
	KeyValuePair_2__ctor_m2890662519(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Key()
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2269785873_gshared (KeyValuePair_2_t4030510692 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_key_0();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Key_m2269785873_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4030510692 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4030510692 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2269785873(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m3685443922_gshared (KeyValuePair_2_t4030510692 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m3685443922_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t4030510692 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4030510692 *>(__this + 1);
	KeyValuePair_2_set_Key_m3685443922(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::get_Value()
extern "C"  ProfileData_t1961690790  KeyValuePair_2_get_Value_m2743302773_gshared (KeyValuePair_2_t4030510692 * __this, const MethodInfo* method)
{
	{
		ProfileData_t1961690790  L_0 = (ProfileData_t1961690790 )__this->get_value_1();
		return L_0;
	}
}
extern "C"  ProfileData_t1961690790  KeyValuePair_2_get_Value_m2743302773_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4030510692 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4030510692 *>(__this + 1);
	return KeyValuePair_2_get_Value_m2743302773(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m3467494482_gshared (KeyValuePair_2_t4030510692 * __this, ProfileData_t1961690790  ___value0, const MethodInfo* method)
{
	{
		ProfileData_t1961690790  L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m3467494482_AdjustorThunk (Il2CppObject * __this, ProfileData_t1961690790  ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t4030510692 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4030510692 *>(__this + 1);
	KeyValuePair_2_set_Value_m3467494482(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,Vuforia.WebCamProfile/ProfileData>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m2516779894_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2516779894_gshared (KeyValuePair_2_t4030510692 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2516779894_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Il2CppObject * V_0 = NULL;
	ProfileData_t1961690790  V_1;
	memset(&V_1, 0, sizeof(V_1));
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		Il2CppObject * L_2 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4030510692 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
		if (!L_2)
		{
			G_B2_0 = 1;
			G_B2_1 = L_1;
			G_B2_2 = L_1;
			goto IL_0039;
		}
	}
	{
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4030510692 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (Il2CppObject *)L_3;
		NullCheck((Il2CppObject *)(*(&V_0)));
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_0)));
		G_B3_0 = L_4;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_5;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_6 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, _stringLiteral1396);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)L_6;
		ProfileData_t1961690790  L_8 = ((  ProfileData_t1961690790  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4030510692 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_7;
		G_B4_2 = L_7;
	}
	{
		ProfileData_t1961690790  L_9 = ((  ProfileData_t1961690790  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t4030510692 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (ProfileData_t1961690790 )L_9;
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 5), (&V_1));
		NullCheck((Il2CppObject *)L_10);
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_10);
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2516779894_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t4030510692 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t4030510692 *>(__this + 1);
	return KeyValuePair_2_ToString_m2516779894(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::.ctor(TKey,TValue)
extern "C"  void KeyValuePair_2__ctor_m663570445_gshared (KeyValuePair_2_t1718082560 * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	{
		int32_t L_0 = ___key0;
		((  void (*) (Il2CppObject *, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1718082560 *)__this), (int32_t)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		Il2CppObject * L_1 = ___value1;
		((  void (*) (Il2CppObject *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1718082560 *)__this), (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1));
		return;
	}
}
extern "C"  void KeyValuePair_2__ctor_m663570445_AdjustorThunk (Il2CppObject * __this, int32_t ___key0, Il2CppObject * ___value1, const MethodInfo* method)
{
	KeyValuePair_2_t1718082560 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1718082560 *>(__this + 1);
	KeyValuePair_2__ctor_m663570445(_thisAdjusted, ___key0, ___value1, method);
}
// TKey System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Key()
extern "C"  int32_t KeyValuePair_2_get_Key_m2589995323_gshared (KeyValuePair_2_t1718082560 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_key_0();
		return L_0;
	}
}
extern "C"  int32_t KeyValuePair_2_get_Key_m2589995323_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1718082560 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1718082560 *>(__this + 1);
	return KeyValuePair_2_get_Key_m2589995323(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::set_Key(TKey)
extern "C"  void KeyValuePair_2_set_Key_m2974711292_gshared (KeyValuePair_2_t1718082560 * __this, int32_t ___value0, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_key_0(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Key_m2974711292_AdjustorThunk (Il2CppObject * __this, int32_t ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1718082560 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1718082560 *>(__this + 1);
	KeyValuePair_2_set_Key_m2974711292(_thisAdjusted, ___value0, method);
}
// TValue System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::get_Value()
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1871736891_gshared (KeyValuePair_2_t1718082560 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_value_1();
		return L_0;
	}
}
extern "C"  Il2CppObject * KeyValuePair_2_get_Value_m1871736891_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1718082560 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1718082560 *>(__this + 1);
	return KeyValuePair_2_get_Value_m1871736891(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::set_Value(TValue)
extern "C"  void KeyValuePair_2_set_Value_m1040348156_gshared (KeyValuePair_2_t1718082560 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		__this->set_value_1(L_0);
		return;
	}
}
extern "C"  void KeyValuePair_2_set_Value_m1040348156_AdjustorThunk (Il2CppObject * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	KeyValuePair_2_t1718082560 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1718082560 *>(__this + 1);
	KeyValuePair_2_set_Value_m1040348156(_thisAdjusted, ___value0, method);
}
// System.String System.Collections.Generic.KeyValuePair`2<Vuforia.Image/PIXEL_FORMAT,System.Object>::ToString()
extern Il2CppClass* StringU5BU5D_t4054002952_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral93;
extern const uint32_t KeyValuePair_2_ToString_m2117123366_MetadataUsageId;
extern "C"  String_t* KeyValuePair_2_ToString_m2117123366_gshared (KeyValuePair_2_t1718082560 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (KeyValuePair_2_ToString_m2117123366_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Il2CppObject * V_1 = NULL;
	int32_t G_B2_0 = 0;
	StringU5BU5D_t4054002952* G_B2_1 = NULL;
	StringU5BU5D_t4054002952* G_B2_2 = NULL;
	int32_t G_B1_0 = 0;
	StringU5BU5D_t4054002952* G_B1_1 = NULL;
	StringU5BU5D_t4054002952* G_B1_2 = NULL;
	String_t* G_B3_0 = NULL;
	int32_t G_B3_1 = 0;
	StringU5BU5D_t4054002952* G_B3_2 = NULL;
	StringU5BU5D_t4054002952* G_B3_3 = NULL;
	int32_t G_B5_0 = 0;
	StringU5BU5D_t4054002952* G_B5_1 = NULL;
	StringU5BU5D_t4054002952* G_B5_2 = NULL;
	int32_t G_B4_0 = 0;
	StringU5BU5D_t4054002952* G_B4_1 = NULL;
	StringU5BU5D_t4054002952* G_B4_2 = NULL;
	String_t* G_B6_0 = NULL;
	int32_t G_B6_1 = 0;
	StringU5BU5D_t4054002952* G_B6_2 = NULL;
	StringU5BU5D_t4054002952* G_B6_3 = NULL;
	{
		StringU5BU5D_t4054002952* L_0 = (StringU5BU5D_t4054002952*)((StringU5BU5D_t4054002952*)SZArrayNew(StringU5BU5D_t4054002952_il2cpp_TypeInfo_var, (uint32_t)5));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral91);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (String_t*)_stringLiteral91);
		StringU5BU5D_t4054002952* L_1 = (StringU5BU5D_t4054002952*)L_0;
		int32_t L_2 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1718082560 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		G_B1_0 = 1;
		G_B1_1 = L_1;
		G_B1_2 = L_1;
	}
	{
		int32_t L_3 = ((  int32_t (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1718082560 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		V_0 = (int32_t)L_3;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 3), (&V_0));
		NullCheck((Il2CppObject *)L_4);
		String_t* L_5 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)L_4);
		G_B3_0 = L_5;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		goto IL_003e;
	}

IL_0039:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B3_0 = L_6;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
	}

IL_003e:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		(G_B3_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B3_1), (String_t*)G_B3_0);
		StringU5BU5D_t4054002952* L_7 = (StringU5BU5D_t4054002952*)G_B3_3;
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, _stringLiteral1396);
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(2), (String_t*)_stringLiteral1396);
		StringU5BU5D_t4054002952* L_8 = (StringU5BU5D_t4054002952*)L_7;
		Il2CppObject * L_9 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1718082560 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		G_B4_0 = 3;
		G_B4_1 = L_8;
		G_B4_2 = L_8;
		if (!L_9)
		{
			G_B5_0 = 3;
			G_B5_1 = L_8;
			G_B5_2 = L_8;
			goto IL_0072;
		}
	}
	{
		Il2CppObject * L_10 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4)->methodPointer)(il2cpp_codegen_fake_box((KeyValuePair_2_t1718082560 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 4));
		V_1 = (Il2CppObject *)L_10;
		NullCheck((Il2CppObject *)(*(&V_1)));
		String_t* L_11 = VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, (Il2CppObject *)(*(&V_1)));
		G_B6_0 = L_11;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		goto IL_0077;
	}

IL_0072:
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		G_B6_0 = L_12;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
	}

IL_0077:
	{
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, G_B6_0);
		(G_B6_2)->SetAt(static_cast<il2cpp_array_size_t>(G_B6_1), (String_t*)G_B6_0);
		StringU5BU5D_t4054002952* L_13 = (StringU5BU5D_t4054002952*)G_B6_3;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, _stringLiteral93);
		(L_13)->SetAt(static_cast<il2cpp_array_size_t>(4), (String_t*)_stringLiteral93);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m21867311(NULL /*static, unused*/, (StringU5BU5D_t4054002952*)L_13, /*hidden argument*/NULL);
		return L_14;
	}
}
extern "C"  String_t* KeyValuePair_2_ToString_m2117123366_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	KeyValuePair_2_t1718082560 * _thisAdjusted = reinterpret_cast<KeyValuePair_2_t1718082560 *>(__this + 1);
	return KeyValuePair_2_ToString_m2117123366(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>)
extern "C"  void Enumerator__ctor_m857368315_gshared (Enumerator_t3528606100 * __this, LinkedList_1_t2045451540 * ___parent0, const MethodInfo* method)
{
	{
		LinkedList_1_t2045451540 * L_0 = ___parent0;
		__this->set_list_0(L_0);
		__this->set_current_1((LinkedListNode_1_t3787551078 *)NULL);
		__this->set_index_2((-1));
		LinkedList_1_t2045451540 * L_1 = ___parent0;
		NullCheck(L_1);
		uint32_t L_2 = (uint32_t)L_1->get_version_1();
		__this->set_version_3(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m857368315_AdjustorThunk (Il2CppObject * __this, LinkedList_1_t2045451540 * ___parent0, const MethodInfo* method)
{
	Enumerator_t3528606100 * _thisAdjusted = reinterpret_cast<Enumerator_t3528606100 *>(__this + 1);
	Enumerator__ctor_m857368315(_thisAdjusted, ___parent0, method);
}
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1753810300_gshared (Enumerator_t3528606100 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ((  Il2CppObject * (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t3528606100 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1753810300_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3528606100 * _thisAdjusted = reinterpret_cast<Enumerator_t3528606100 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1753810300(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3204776971;
extern const uint32_t Enumerator_System_Collections_IEnumerator_Reset_m4062113552_MetadataUsageId;
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4062113552_gshared (Enumerator_t3528606100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_Reset_m4062113552_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2045451540 * L_0 = (LinkedList_1_t2045451540 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1794727681 * L_1 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (uint32_t)__this->get_version_3();
		LinkedList_1_t2045451540 * L_3 = (LinkedList_1_t2045451540 *)__this->get_list_0();
		NullCheck(L_3);
		uint32_t L_4 = (uint32_t)L_3->get_version_1();
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_5 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_5, (String_t*)_stringLiteral3204776971, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		__this->set_current_1((LinkedListNode_1_t3787551078 *)NULL);
		__this->set_index_2((-1));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4062113552_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3528606100 * _thisAdjusted = reinterpret_cast<Enumerator_t3528606100 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m4062113552(_thisAdjusted, method);
}
// T System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::get_Current()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_get_Current_m1124073047_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_get_Current_m1124073047_gshared (Enumerator_t3528606100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_get_Current_m1124073047_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2045451540 * L_0 = (LinkedList_1_t2045451540 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1794727681 * L_1 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		LinkedListNode_1_t3787551078 * L_2 = (LinkedListNode_1_t3787551078 *)__this->get_current_1();
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		LinkedListNode_1_t3787551078 * L_4 = (LinkedListNode_1_t3787551078 *)__this->get_current_1();
		NullCheck((LinkedListNode_1_t3787551078 *)L_4);
		Il2CppObject * L_5 = ((  Il2CppObject * (*) (LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((LinkedListNode_1_t3787551078 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_5;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m1124073047_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3528606100 * _thisAdjusted = reinterpret_cast<Enumerator_t3528606100 *>(__this + 1);
	return Enumerator_get_Current_m1124073047(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::MoveNext()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3204776971;
extern const uint32_t Enumerator_MoveNext_m2358966120_MetadataUsageId;
extern "C"  bool Enumerator_MoveNext_m2358966120_gshared (Enumerator_t3528606100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_MoveNext_m2358966120_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2045451540 * L_0 = (LinkedList_1_t2045451540 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1794727681 * L_1 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (uint32_t)__this->get_version_3();
		LinkedList_1_t2045451540 * L_3 = (LinkedList_1_t2045451540 *)__this->get_list_0();
		NullCheck(L_3);
		uint32_t L_4 = (uint32_t)L_3->get_version_1();
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_5 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_5, (String_t*)_stringLiteral3204776971, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		LinkedListNode_1_t3787551078 * L_6 = (LinkedListNode_1_t3787551078 *)__this->get_current_1();
		if (L_6)
		{
			goto IL_0054;
		}
	}
	{
		LinkedList_1_t2045451540 * L_7 = (LinkedList_1_t2045451540 *)__this->get_list_0();
		NullCheck(L_7);
		LinkedListNode_1_t3787551078 * L_8 = (LinkedListNode_1_t3787551078 *)L_7->get_first_3();
		__this->set_current_1(L_8);
		goto IL_0082;
	}

IL_0054:
	{
		LinkedListNode_1_t3787551078 * L_9 = (LinkedListNode_1_t3787551078 *)__this->get_current_1();
		NullCheck(L_9);
		LinkedListNode_1_t3787551078 * L_10 = (LinkedListNode_1_t3787551078 *)L_9->get_forward_2();
		__this->set_current_1(L_10);
		LinkedListNode_1_t3787551078 * L_11 = (LinkedListNode_1_t3787551078 *)__this->get_current_1();
		LinkedList_1_t2045451540 * L_12 = (LinkedList_1_t2045451540 *)__this->get_list_0();
		NullCheck(L_12);
		LinkedListNode_1_t3787551078 * L_13 = (LinkedListNode_1_t3787551078 *)L_12->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_11) == ((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_13))))
		{
			goto IL_0082;
		}
	}
	{
		__this->set_current_1((LinkedListNode_1_t3787551078 *)NULL);
	}

IL_0082:
	{
		LinkedListNode_1_t3787551078 * L_14 = (LinkedListNode_1_t3787551078 *)__this->get_current_1();
		if (L_14)
		{
			goto IL_0096;
		}
	}
	{
		__this->set_index_2((-1));
		return (bool)0;
	}

IL_0096:
	{
		int32_t L_15 = (int32_t)__this->get_index_2();
		__this->set_index_2(((int32_t)((int32_t)L_15+(int32_t)1)));
		return (bool)1;
	}
}
extern "C"  bool Enumerator_MoveNext_m2358966120_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3528606100 * _thisAdjusted = reinterpret_cast<Enumerator_t3528606100 *>(__this + 1);
	return Enumerator_MoveNext_m2358966120(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<System.Object>::Dispose()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_Dispose_m272587367_MetadataUsageId;
extern "C"  void Enumerator_Dispose_m272587367_gshared (Enumerator_t3528606100 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_Dispose_m272587367_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t2045451540 * L_0 = (LinkedList_1_t2045451540 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1794727681 * L_1 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		__this->set_current_1((LinkedListNode_1_t3787551078 *)NULL);
		__this->set_list_0((LinkedList_1_t2045451540 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m272587367_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t3528606100 * _thisAdjusted = reinterpret_cast<Enumerator_t3528606100 *>(__this + 1);
	Enumerator_Dispose_m272587367(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManagerImpl/IdPair>::.ctor(System.Collections.Generic.LinkedList`1<T>)
extern "C"  void Enumerator__ctor_m4100598954_gshared (Enumerator_t2880376494 * __this, LinkedList_1_t1397221934 * ___parent0, const MethodInfo* method)
{
	{
		LinkedList_1_t1397221934 * L_0 = ___parent0;
		__this->set_list_0(L_0);
		__this->set_current_1((LinkedListNode_1_t3139321472 *)NULL);
		__this->set_index_2((-1));
		LinkedList_1_t1397221934 * L_1 = ___parent0;
		NullCheck(L_1);
		uint32_t L_2 = (uint32_t)L_1->get_version_1();
		__this->set_version_3(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m4100598954_AdjustorThunk (Il2CppObject * __this, LinkedList_1_t1397221934 * ___parent0, const MethodInfo* method)
{
	Enumerator_t2880376494 * _thisAdjusted = reinterpret_cast<Enumerator_t2880376494 *>(__this + 1);
	Enumerator__ctor_m4100598954(_thisAdjusted, ___parent0, method);
}
// System.Object System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.IEnumerator.get_Current()
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3895708397_gshared (Enumerator_t2880376494 * __this, const MethodInfo* method)
{
	{
		IdPair_t3522586765  L_0 = ((  IdPair_t3522586765  (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t2880376494 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		IdPair_t3522586765  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_1);
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3895708397_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2880376494 * _thisAdjusted = reinterpret_cast<Enumerator_t2880376494 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3895708397(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.IEnumerator.Reset()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3204776971;
extern const uint32_t Enumerator_System_Collections_IEnumerator_Reset_m1947328705_MetadataUsageId;
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1947328705_gshared (Enumerator_t2880376494 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_Reset_m1947328705_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t1397221934 * L_0 = (LinkedList_1_t1397221934 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1794727681 * L_1 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (uint32_t)__this->get_version_3();
		LinkedList_1_t1397221934 * L_3 = (LinkedList_1_t1397221934 *)__this->get_list_0();
		NullCheck(L_3);
		uint32_t L_4 = (uint32_t)L_3->get_version_1();
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_5 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_5, (String_t*)_stringLiteral3204776971, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		__this->set_current_1((LinkedListNode_1_t3139321472 *)NULL);
		__this->set_index_2((-1));
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1947328705_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2880376494 * _thisAdjusted = reinterpret_cast<Enumerator_t2880376494 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1947328705(_thisAdjusted, method);
}
// T System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManagerImpl/IdPair>::get_Current()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_get_Current_m1106402565_MetadataUsageId;
extern "C"  IdPair_t3522586765  Enumerator_get_Current_m1106402565_gshared (Enumerator_t2880376494 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_get_Current_m1106402565_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t1397221934 * L_0 = (LinkedList_1_t1397221934 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1794727681 * L_1 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		LinkedListNode_1_t3139321472 * L_2 = (LinkedListNode_1_t3139321472 *)__this->get_current_1();
		if (L_2)
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_3 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_3, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_3);
	}

IL_0023:
	{
		LinkedListNode_1_t3139321472 * L_4 = (LinkedListNode_1_t3139321472 *)__this->get_current_1();
		NullCheck((LinkedListNode_1_t3139321472 *)L_4);
		IdPair_t3522586765  L_5 = ((  IdPair_t3522586765  (*) (LinkedListNode_1_t3139321472 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2)->methodPointer)((LinkedListNode_1_t3139321472 *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2));
		return L_5;
	}
}
extern "C"  IdPair_t3522586765  Enumerator_get_Current_m1106402565_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2880376494 * _thisAdjusted = reinterpret_cast<Enumerator_t2880376494 *>(__this + 1);
	return Enumerator_get_Current_m1106402565(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManagerImpl/IdPair>::MoveNext()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3204776971;
extern const uint32_t Enumerator_MoveNext_m99978027_MetadataUsageId;
extern "C"  bool Enumerator_MoveNext_m99978027_gshared (Enumerator_t2880376494 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_MoveNext_m99978027_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t1397221934 * L_0 = (LinkedList_1_t1397221934 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1794727681 * L_1 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		uint32_t L_2 = (uint32_t)__this->get_version_3();
		LinkedList_1_t1397221934 * L_3 = (LinkedList_1_t1397221934 *)__this->get_list_0();
		NullCheck(L_3);
		uint32_t L_4 = (uint32_t)L_3->get_version_1();
		if ((((int32_t)L_2) == ((int32_t)L_4)))
		{
			goto IL_0033;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_5 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_5, (String_t*)_stringLiteral3204776971, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0033:
	{
		LinkedListNode_1_t3139321472 * L_6 = (LinkedListNode_1_t3139321472 *)__this->get_current_1();
		if (L_6)
		{
			goto IL_0054;
		}
	}
	{
		LinkedList_1_t1397221934 * L_7 = (LinkedList_1_t1397221934 *)__this->get_list_0();
		NullCheck(L_7);
		LinkedListNode_1_t3139321472 * L_8 = (LinkedListNode_1_t3139321472 *)L_7->get_first_3();
		__this->set_current_1(L_8);
		goto IL_0082;
	}

IL_0054:
	{
		LinkedListNode_1_t3139321472 * L_9 = (LinkedListNode_1_t3139321472 *)__this->get_current_1();
		NullCheck(L_9);
		LinkedListNode_1_t3139321472 * L_10 = (LinkedListNode_1_t3139321472 *)L_9->get_forward_2();
		__this->set_current_1(L_10);
		LinkedListNode_1_t3139321472 * L_11 = (LinkedListNode_1_t3139321472 *)__this->get_current_1();
		LinkedList_1_t1397221934 * L_12 = (LinkedList_1_t1397221934 *)__this->get_list_0();
		NullCheck(L_12);
		LinkedListNode_1_t3139321472 * L_13 = (LinkedListNode_1_t3139321472 *)L_12->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t3139321472 *)L_11) == ((Il2CppObject*)(LinkedListNode_1_t3139321472 *)L_13))))
		{
			goto IL_0082;
		}
	}
	{
		__this->set_current_1((LinkedListNode_1_t3139321472 *)NULL);
	}

IL_0082:
	{
		LinkedListNode_1_t3139321472 * L_14 = (LinkedListNode_1_t3139321472 *)__this->get_current_1();
		if (L_14)
		{
			goto IL_0096;
		}
	}
	{
		__this->set_index_2((-1));
		return (bool)0;
	}

IL_0096:
	{
		int32_t L_15 = (int32_t)__this->get_index_2();
		__this->set_index_2(((int32_t)((int32_t)L_15+(int32_t)1)));
		return (bool)1;
	}
}
extern "C"  bool Enumerator_MoveNext_m99978027_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2880376494 * _thisAdjusted = reinterpret_cast<Enumerator_t2880376494 *>(__this + 1);
	return Enumerator_MoveNext_m99978027(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1/Enumerator<Vuforia.VuforiaManagerImpl/IdPair>::Dispose()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_Dispose_m3119627030_MetadataUsageId;
extern "C"  void Enumerator_Dispose_m3119627030_gshared (Enumerator_t2880376494 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_Dispose_m3119627030_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedList_1_t1397221934 * L_0 = (LinkedList_1_t1397221934 *)__this->get_list_0();
		if (L_0)
		{
			goto IL_0012;
		}
	}
	{
		ObjectDisposedException_t1794727681 * L_1 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_1, (String_t*)NULL, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0012:
	{
		__this->set_current_1((LinkedListNode_1_t3139321472 *)NULL);
		__this->set_list_0((LinkedList_1_t1397221934 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3119627030_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2880376494 * _thisAdjusted = reinterpret_cast<Enumerator_t2880376494 *>(__this + 1);
	Enumerator_Dispose_m3119627030(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LinkedList_1__ctor_m2955457271_MetadataUsageId;
extern "C"  void LinkedList_1__ctor_m2955457271_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1__ctor_m2955457271_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_syncRoot_2(L_0);
		__this->set_first_3((LinkedListNode_1_t3787551078 *)NULL);
		int32_t L_1 = (int32_t)0;
		V_0 = (uint32_t)L_1;
		__this->set_version_1(L_1);
		uint32_t L_2 = V_0;
		__this->set_count_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LinkedList_1__ctor_m3369579448_MetadataUsageId;
extern "C"  void LinkedList_1__ctor_m3369579448_gshared (LinkedList_1_t2045451540 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1__ctor_m3369579448_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((LinkedList_1_t2045451540 *)__this);
		((  void (*) (LinkedList_1_t2045451540 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((LinkedList_1_t2045451540 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		SerializationInfo_t2185721892 * L_0 = ___info0;
		__this->set_si_4(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_1, /*hidden argument*/NULL);
		__this->set_syncRoot_2(L_1);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m3576108392_gshared (LinkedList_1_t2045451540 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = ___value0;
		NullCheck((LinkedList_1_t2045451540 *)__this);
		((  LinkedListNode_1_t3787551078 * (*) (LinkedList_1_t2045451540 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((LinkedList_1_t2045451540 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93090393;
extern const uint32_t LinkedList_1_System_Collections_ICollection_CopyTo_m2331638317_MetadataUsageId;
extern "C"  void LinkedList_1_System_Collections_ICollection_CopyTo_m2331638317_gshared (LinkedList_1_t2045451540 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_System_Collections_ICollection_CopyTo_m2331638317_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t1108656482* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		ObjectU5BU5D_t1108656482* L_1 = V_0;
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentException_t928607144 * L_2 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral93090393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		ObjectU5BU5D_t1108656482* L_3 = V_0;
		int32_t L_4 = ___index1;
		NullCheck((LinkedList_1_t2045451540 *)__this);
		((  void (*) (LinkedList_1_t2045451540 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((LinkedList_1_t2045451540 *)__this, (ObjectU5BU5D_t1108656482*)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m2027502985_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		NullCheck((LinkedList_1_t2045451540 *)__this);
		Enumerator_t3528606100  L_0 = ((  Enumerator_t3528606100  (*) (LinkedList_1_t2045451540 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((LinkedList_1_t2045451540 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Enumerator_t3528606100  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m51916412_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		NullCheck((LinkedList_1_t2045451540 *)__this);
		Enumerator_t3528606100  L_0 = ((  Enumerator_t3528606100  (*) (LinkedList_1_t2045451540 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((LinkedList_1_t2045451540 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Enumerator_t3528606100  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m2230847288_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.LinkedList`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m573420165_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_2();
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3386882;
extern const uint32_t LinkedList_1_VerifyReferencedNode_m3939775124_MetadataUsageId;
extern "C"  void LinkedList_1_VerifyReferencedNode_m3939775124_gshared (LinkedList_1_t2045451540 * __this, LinkedListNode_1_t3787551078 * ___node0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_VerifyReferencedNode_m3939775124_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedListNode_1_t3787551078 * L_0 = ___node0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral3386882, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		LinkedListNode_1_t3787551078 * L_2 = ___node0;
		NullCheck((LinkedListNode_1_t3787551078 *)L_2);
		LinkedList_1_t2045451540 * L_3 = ((  LinkedList_1_t2045451540 * (*) (LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((LinkedListNode_1_t3787551078 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((Il2CppObject*)(LinkedList_1_t2045451540 *)L_3) == ((Il2CppObject*)(LinkedList_1_t2045451540 *)__this)))
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_4 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::AddLast(T)
extern "C"  LinkedListNode_1_t3787551078 * LinkedList_1_AddLast_m4070107716_gshared (LinkedList_1_t2045451540 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t3787551078 * V_0 = NULL;
	{
		LinkedListNode_1_t3787551078 * L_0 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		Il2CppObject * L_1 = ___value0;
		LinkedListNode_1_t3787551078 * L_2 = (LinkedListNode_1_t3787551078 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (LinkedListNode_1_t3787551078 *, LinkedList_1_t2045451540 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_2, (LinkedList_1_t2045451540 *)__this, (Il2CppObject *)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_0 = (LinkedListNode_1_t3787551078 *)L_2;
		LinkedListNode_1_t3787551078 * L_3 = V_0;
		__this->set_first_3(L_3);
		goto IL_0038;
	}

IL_001f:
	{
		Il2CppObject * L_4 = ___value0;
		LinkedListNode_1_t3787551078 * L_5 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		NullCheck(L_5);
		LinkedListNode_1_t3787551078 * L_6 = (LinkedListNode_1_t3787551078 *)L_5->get_back_3();
		LinkedListNode_1_t3787551078 * L_7 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		LinkedListNode_1_t3787551078 * L_8 = (LinkedListNode_1_t3787551078 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (LinkedListNode_1_t3787551078 *, LinkedList_1_t2045451540 *, Il2CppObject *, LinkedListNode_1_t3787551078 *, LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(L_8, (LinkedList_1_t2045451540 *)__this, (Il2CppObject *)L_4, (LinkedListNode_1_t3787551078 *)L_6, (LinkedListNode_1_t3787551078 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		V_0 = (LinkedListNode_1_t3787551078 *)L_8;
	}

IL_0038:
	{
		uint32_t L_9 = (uint32_t)__this->get_count_0();
		__this->set_count_0(((int32_t)((int32_t)L_9+(int32_t)1)));
		uint32_t L_10 = (uint32_t)__this->get_version_1();
		__this->set_version_1(((int32_t)((int32_t)L_10+(int32_t)1)));
		LinkedListNode_1_t3787551078 * L_11 = V_0;
		return L_11;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Clear()
extern "C"  void LinkedList_1_Clear_m361590562_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		goto IL_000b;
	}

IL_0005:
	{
		NullCheck((LinkedList_1_t2045451540 *)__this);
		((  void (*) (LinkedList_1_t2045451540 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((LinkedList_1_t2045451540 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_000b:
	{
		LinkedListNode_1_t3787551078 * L_0 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Contains(T)
extern "C"  bool LinkedList_1_Contains_m3484410556_gshared (LinkedList_1_t2045451540 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t3787551078 * V_0 = NULL;
	{
		LinkedListNode_1_t3787551078 * L_0 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t3787551078 *)L_0;
		LinkedListNode_1_t3787551078 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		LinkedListNode_1_t3787551078 * L_2 = V_0;
		NullCheck((LinkedListNode_1_t3787551078 *)L_2);
		Il2CppObject * L_3 = ((  Il2CppObject * (*) (LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t3787551078 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck((Il2CppObject *)(*(&___value0)));
		bool L_4 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*(&___value0)), (Il2CppObject *)L_3);
		if (!L_4)
		{
			goto IL_002e;
		}
	}
	{
		return (bool)1;
	}

IL_002e:
	{
		LinkedListNode_1_t3787551078 * L_5 = V_0;
		NullCheck(L_5);
		LinkedListNode_1_t3787551078 * L_6 = (LinkedListNode_1_t3787551078 *)L_5->get_forward_2();
		V_0 = (LinkedListNode_1_t3787551078 *)L_6;
		LinkedListNode_1_t3787551078 * L_7 = V_0;
		LinkedListNode_1_t3787551078 * L_8 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_7) == ((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_8))))
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::CopyTo(T[],System.Int32)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93090393;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern Il2CppCodeGenString* _stringLiteral279574791;
extern Il2CppCodeGenString* _stringLiteral768919341;
extern const uint32_t LinkedList_1_CopyTo_m3470139544_MetadataUsageId;
extern "C"  void LinkedList_1_CopyTo_m3470139544_gshared (LinkedList_1_t2045451540 * __this, ObjectU5BU5D_t1108656482* ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_CopyTo_m3470139544_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LinkedListNode_1_t3787551078 * V_0 = NULL;
	{
		ObjectU5BU5D_t1108656482* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral93090393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index1;
		ObjectU5BU5D_t1108656482* L_3 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_3);
		int32_t L_4 = Array_GetLowerBound_m2369136542((Il2CppArray *)(Il2CppArray *)L_3, (int32_t)0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) < ((uint32_t)L_4))))
		{
			goto IL_0029;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_5 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_5, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0029:
	{
		ObjectU5BU5D_t1108656482* L_6 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_6);
		int32_t L_7 = Array_get_Rank_m1671008509((Il2CppArray *)(Il2CppArray *)L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)1)))
		{
			goto IL_0045;
		}
	}
	{
		ArgumentException_t928607144 * L_8 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_8, (String_t*)_stringLiteral93090393, (String_t*)_stringLiteral279574791, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0045:
	{
		ObjectU5BU5D_t1108656482* L_9 = ___array0;
		NullCheck(L_9);
		int32_t L_10 = ___index1;
		ObjectU5BU5D_t1108656482* L_11 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_11);
		int32_t L_12 = Array_GetLowerBound_m2369136542((Il2CppArray *)(Il2CppArray *)L_11, (int32_t)0, /*hidden argument*/NULL);
		uint32_t L_13 = (uint32_t)__this->get_count_0();
		if ((((int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))-(int32_t)L_10))+(int32_t)L_12)))))) >= ((int64_t)(((int64_t)((uint64_t)L_13))))))
		{
			goto IL_006a;
		}
	}
	{
		ArgumentException_t928607144 * L_14 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_14, (String_t*)_stringLiteral768919341, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_006a:
	{
		LinkedListNode_1_t3787551078 * L_15 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t3787551078 *)L_15;
		LinkedListNode_1_t3787551078 * L_16 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		if (L_16)
		{
			goto IL_007d;
		}
	}
	{
		return;
	}

IL_007d:
	{
		ObjectU5BU5D_t1108656482* L_17 = ___array0;
		int32_t L_18 = ___index1;
		LinkedListNode_1_t3787551078 * L_19 = V_0;
		NullCheck((LinkedListNode_1_t3787551078 *)L_19);
		Il2CppObject * L_20 = ((  Il2CppObject * (*) (LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t3787551078 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (Il2CppObject *)L_20);
		int32_t L_21 = ___index1;
		___index1 = (int32_t)((int32_t)((int32_t)L_21+(int32_t)1));
		LinkedListNode_1_t3787551078 * L_22 = V_0;
		NullCheck(L_22);
		LinkedListNode_1_t3787551078 * L_23 = (LinkedListNode_1_t3787551078 *)L_22->get_forward_2();
		V_0 = (LinkedListNode_1_t3787551078 *)L_23;
		LinkedListNode_1_t3787551078 * L_24 = V_0;
		LinkedListNode_1_t3787551078 * L_25 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_24) == ((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_25))))
		{
			goto IL_007d;
		}
	}
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::Find(T)
extern "C"  LinkedListNode_1_t3787551078 * LinkedList_1_Find_m2643247334_gshared (LinkedList_1_t2045451540 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t3787551078 * V_0 = NULL;
	{
		LinkedListNode_1_t3787551078 * L_0 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t3787551078 *)L_0;
		LinkedListNode_1_t3787551078 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t3787551078 *)NULL;
	}

IL_000f:
	{
		Il2CppObject * L_2 = ___value0;
		if (L_2)
		{
			goto IL_002a;
		}
	}
	{
		LinkedListNode_1_t3787551078 * L_3 = V_0;
		NullCheck((LinkedListNode_1_t3787551078 *)L_3);
		Il2CppObject * L_4 = ((  Il2CppObject * (*) (LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t3787551078 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		if (!L_4)
		{
			goto IL_0052;
		}
	}

IL_002a:
	{
		Il2CppObject * L_5 = ___value0;
		if (!L_5)
		{
			goto IL_0054;
		}
	}
	{
		LinkedListNode_1_t3787551078 * L_6 = V_0;
		NullCheck((LinkedListNode_1_t3787551078 *)L_6);
		Il2CppObject * L_7 = ((  Il2CppObject * (*) (LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t3787551078 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck((Il2CppObject *)(*(&___value0)));
		bool L_8 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)(*(&___value0)), (Il2CppObject *)L_7);
		if (!L_8)
		{
			goto IL_0054;
		}
	}

IL_0052:
	{
		LinkedListNode_1_t3787551078 * L_9 = V_0;
		return L_9;
	}

IL_0054:
	{
		LinkedListNode_1_t3787551078 * L_10 = V_0;
		NullCheck(L_10);
		LinkedListNode_1_t3787551078 * L_11 = (LinkedListNode_1_t3787551078 *)L_10->get_forward_2();
		V_0 = (LinkedListNode_1_t3787551078 *)L_11;
		LinkedListNode_1_t3787551078 * L_12 = V_0;
		LinkedListNode_1_t3787551078 * L_13 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_12) == ((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_13))))
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t3787551078 *)NULL;
	}
}
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<System.Object>::GetEnumerator()
extern "C"  Enumerator_t3528606100  LinkedList_1_GetEnumerator_m3713737734_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		Enumerator_t3528606100  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m857368315(&L_0, (LinkedList_1_t2045451540 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1612836015;
extern Il2CppCodeGenString* _stringLiteral351608024;
extern const uint32_t LinkedList_1_GetObjectData_m3974480661_MetadataUsageId;
extern "C"  void LinkedList_1_GetObjectData_m3974480661_gshared (LinkedList_1_t2045451540 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_GetObjectData_m3974480661_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t1108656482* V_0 = NULL;
	{
		uint32_t L_0 = (uint32_t)__this->get_count_0();
		V_0 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), (uint32_t)(((uintptr_t)L_0))));
		ObjectU5BU5D_t1108656482* L_1 = V_0;
		NullCheck((LinkedList_1_t2045451540 *)__this);
		((  void (*) (LinkedList_1_t2045451540 *, ObjectU5BU5D_t1108656482*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((LinkedList_1_t2045451540 *)__this, (ObjectU5BU5D_t1108656482*)L_1, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		SerializationInfo_t2185721892 * L_2 = ___info0;
		ObjectU5BU5D_t1108656482* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t2185721892 *)L_2);
		SerializationInfo_AddValue_m3341936982((SerializationInfo_t2185721892 *)L_2, (String_t*)_stringLiteral1612836015, (Il2CppObject *)(Il2CppObject *)L_3, (Type_t *)L_4, /*hidden argument*/NULL);
		SerializationInfo_t2185721892 * L_5 = ___info0;
		uint32_t L_6 = (uint32_t)__this->get_version_1();
		NullCheck((SerializationInfo_t2185721892 *)L_5);
		SerializationInfo_AddValue_m787539465((SerializationInfo_t2185721892 *)L_5, (String_t*)_stringLiteral351608024, (uint32_t)L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::OnDeserialization(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1612836015;
extern Il2CppCodeGenString* _stringLiteral351608024;
extern const uint32_t LinkedList_1_OnDeserialization_m3445006959_MetadataUsageId;
extern "C"  void LinkedList_1_OnDeserialization_m3445006959_gshared (LinkedList_1_t2045451540 * __this, Il2CppObject * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_OnDeserialization_m3445006959_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t1108656482* V_0 = NULL;
	Il2CppObject * V_1 = NULL;
	ObjectU5BU5D_t1108656482* V_2 = NULL;
	int32_t V_3 = 0;
	{
		SerializationInfo_t2185721892 * L_0 = (SerializationInfo_t2185721892 *)__this->get_si_4();
		if (!L_0)
		{
			goto IL_0074;
		}
	}
	{
		SerializationInfo_t2185721892 * L_1 = (SerializationInfo_t2185721892 *)__this->get_si_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t2185721892 *)L_1);
		Il2CppObject * L_3 = SerializationInfo_GetValue_m4125471336((SerializationInfo_t2185721892 *)L_1, (String_t*)_stringLiteral1612836015, (Type_t *)L_2, /*hidden argument*/NULL);
		V_0 = (ObjectU5BU5D_t1108656482*)((ObjectU5BU5D_t1108656482*)Castclass(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		ObjectU5BU5D_t1108656482* L_4 = V_0;
		if (!L_4)
		{
			goto IL_0057;
		}
	}
	{
		ObjectU5BU5D_t1108656482* L_5 = V_0;
		V_2 = (ObjectU5BU5D_t1108656482*)L_5;
		V_3 = (int32_t)0;
		goto IL_004e;
	}

IL_003a:
	{
		ObjectU5BU5D_t1108656482* L_6 = V_2;
		int32_t L_7 = V_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (Il2CppObject *)L_9;
		Il2CppObject * L_10 = V_1;
		NullCheck((LinkedList_1_t2045451540 *)__this);
		((  LinkedListNode_1_t3787551078 * (*) (LinkedList_1_t2045451540 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((LinkedList_1_t2045451540 *)__this, (Il2CppObject *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		int32_t L_11 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_12 = V_3;
		ObjectU5BU5D_t1108656482* L_13 = V_2;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_003a;
		}
	}

IL_0057:
	{
		SerializationInfo_t2185721892 * L_14 = (SerializationInfo_t2185721892 *)__this->get_si_4();
		NullCheck((SerializationInfo_t2185721892 *)L_14);
		uint32_t L_15 = SerializationInfo_GetUInt32_m1908270281((SerializationInfo_t2185721892 *)L_14, (String_t*)_stringLiteral351608024, /*hidden argument*/NULL);
		__this->set_version_1(L_15);
		__this->set_si_4((SerializationInfo_t2185721892 *)NULL);
	}

IL_0074:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<System.Object>::Remove(T)
extern "C"  bool LinkedList_1_Remove_m3283493303_gshared (LinkedList_1_t2045451540 * __this, Il2CppObject * ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t3787551078 * V_0 = NULL;
	{
		Il2CppObject * L_0 = ___value0;
		NullCheck((LinkedList_1_t2045451540 *)__this);
		LinkedListNode_1_t3787551078 * L_1 = ((  LinkedListNode_1_t3787551078 * (*) (LinkedList_1_t2045451540 *, Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((LinkedList_1_t2045451540 *)__this, (Il2CppObject *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		V_0 = (LinkedListNode_1_t3787551078 *)L_1;
		LinkedListNode_1_t3787551078 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0010;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		LinkedListNode_1_t3787551078 * L_3 = V_0;
		NullCheck((LinkedList_1_t2045451540 *)__this);
		((  void (*) (LinkedList_1_t2045451540 *, LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((LinkedList_1_t2045451540 *)__this, (LinkedListNode_1_t3787551078 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		return (bool)1;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedList_1_Remove_m4034790180_gshared (LinkedList_1_t2045451540 * __this, LinkedListNode_1_t3787551078 * ___node0, const MethodInfo* method)
{
	{
		LinkedListNode_1_t3787551078 * L_0 = ___node0;
		NullCheck((LinkedList_1_t2045451540 *)__this);
		((  void (*) (LinkedList_1_t2045451540 *, LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((LinkedList_1_t2045451540 *)__this, (LinkedListNode_1_t3787551078 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		uint32_t L_1 = (uint32_t)__this->get_count_0();
		__this->set_count_0(((int32_t)((int32_t)L_1-(int32_t)1)));
		uint32_t L_2 = (uint32_t)__this->get_count_0();
		if (L_2)
		{
			goto IL_0027;
		}
	}
	{
		__this->set_first_3((LinkedListNode_1_t3787551078 *)NULL);
	}

IL_0027:
	{
		LinkedListNode_1_t3787551078 * L_3 = ___node0;
		LinkedListNode_1_t3787551078 * L_4 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_3) == ((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_4))))
		{
			goto IL_0044;
		}
	}
	{
		LinkedListNode_1_t3787551078 * L_5 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		NullCheck(L_5);
		LinkedListNode_1_t3787551078 * L_6 = (LinkedListNode_1_t3787551078 *)L_5->get_forward_2();
		__this->set_first_3(L_6);
	}

IL_0044:
	{
		uint32_t L_7 = (uint32_t)__this->get_version_1();
		__this->set_version_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		LinkedListNode_1_t3787551078 * L_8 = ___node0;
		NullCheck((LinkedListNode_1_t3787551078 *)L_8);
		((  void (*) (LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((LinkedListNode_1_t3787551078 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<System.Object>::RemoveLast()
extern "C"  void LinkedList_1_RemoveLast_m2573038887_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		LinkedListNode_1_t3787551078 * L_0 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		LinkedListNode_1_t3787551078 * L_1 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		NullCheck(L_1);
		LinkedListNode_1_t3787551078 * L_2 = (LinkedListNode_1_t3787551078 *)L_1->get_back_3();
		NullCheck((LinkedList_1_t2045451540 *)__this);
		((  void (*) (LinkedList_1_t2045451540 *, LinkedListNode_1_t3787551078 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((LinkedList_1_t2045451540 *)__this, (LinkedListNode_1_t3787551078 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_001c:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.LinkedList`1<System.Object>::get_Count()
extern "C"  int32_t LinkedList_1_get_Count_m1368924491_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = (uint32_t)__this->get_count_0();
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<System.Object>::get_First()
extern "C"  LinkedListNode_1_t3787551078 * LinkedList_1_get_First_m3278587786_gshared (LinkedList_1_t2045451540 * __this, const MethodInfo* method)
{
	{
		LinkedListNode_1_t3787551078 * L_0 = (LinkedListNode_1_t3787551078 *)__this->get_first_3();
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManagerImpl/IdPair>::.ctor()
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LinkedList_1__ctor_m2874021140_MetadataUsageId;
extern "C"  void LinkedList_1__ctor_m2874021140_gshared (LinkedList_1_t1397221934 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1__ctor_m2874021140_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		Il2CppObject * L_0 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_0, /*hidden argument*/NULL);
		__this->set_syncRoot_2(L_0);
		__this->set_first_3((LinkedListNode_1_t3139321472 *)NULL);
		int32_t L_1 = (int32_t)0;
		V_0 = (uint32_t)L_1;
		__this->set_version_1(L_1);
		uint32_t L_2 = V_0;
		__this->set_count_0(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManagerImpl/IdPair>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* Il2CppObject_il2cpp_TypeInfo_var;
extern const uint32_t LinkedList_1__ctor_m1464870695_MetadataUsageId;
extern "C"  void LinkedList_1__ctor_m1464870695_gshared (LinkedList_1_t1397221934 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1__ctor_m1464870695_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NullCheck((LinkedList_1_t1397221934 *)__this);
		((  void (*) (LinkedList_1_t1397221934 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0)->methodPointer)((LinkedList_1_t1397221934 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 0));
		SerializationInfo_t2185721892 * L_0 = ___info0;
		__this->set_si_4(L_0);
		Il2CppObject * L_1 = (Il2CppObject *)il2cpp_codegen_object_new(Il2CppObject_il2cpp_TypeInfo_var);
		Object__ctor_m1772956182(L_1, /*hidden argument*/NULL);
		__this->set_syncRoot_2(L_1);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.Generic.ICollection<T>.Add(T)
extern "C"  void LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m2150364761_gshared (LinkedList_1_t1397221934 * __this, IdPair_t3522586765  ___value0, const MethodInfo* method)
{
	{
		IdPair_t3522586765  L_0 = ___value0;
		NullCheck((LinkedList_1_t1397221934 *)__this);
		((  LinkedListNode_1_t3139321472 * (*) (LinkedList_1_t1397221934 *, IdPair_t3522586765 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((LinkedList_1_t1397221934 *)__this, (IdPair_t3522586765 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93090393;
extern const uint32_t LinkedList_1_System_Collections_ICollection_CopyTo_m1076418846_MetadataUsageId;
extern "C"  void LinkedList_1_System_Collections_ICollection_CopyTo_m1076418846_gshared (LinkedList_1_t1397221934 * __this, Il2CppArray * ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_System_Collections_ICollection_CopyTo_m1076418846_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IdPairU5BU5D_t296135328* V_0 = NULL;
	{
		Il2CppArray * L_0 = ___array0;
		V_0 = (IdPairU5BU5D_t296135328*)((IdPairU5BU5D_t296135328*)IsInst(L_0, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		IdPairU5BU5D_t296135328* L_1 = V_0;
		if (L_1)
		{
			goto IL_0018;
		}
	}
	{
		ArgumentException_t928607144 * L_2 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_2, (String_t*)_stringLiteral93090393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_2);
	}

IL_0018:
	{
		IdPairU5BU5D_t296135328* L_3 = V_0;
		int32_t L_4 = ___index1;
		NullCheck((LinkedList_1_t1397221934 *)__this);
		((  void (*) (LinkedList_1_t1397221934 *, IdPairU5BU5D_t296135328*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((LinkedList_1_t1397221934 *)__this, (IdPairU5BU5D_t296135328*)L_3, (int32_t)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C"  Il2CppObject* LinkedList_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m3792814520_gshared (LinkedList_1_t1397221934 * __this, const MethodInfo* method)
{
	{
		NullCheck((LinkedList_1_t1397221934 *)__this);
		Enumerator_t2880376494  L_0 = ((  Enumerator_t2880376494  (*) (LinkedList_1_t1397221934 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((LinkedList_1_t1397221934 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Enumerator_t2880376494  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_1);
		return (Il2CppObject*)L_2;
	}
}
// System.Collections.IEnumerator System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.IEnumerable.GetEnumerator()
extern "C"  Il2CppObject * LinkedList_1_System_Collections_IEnumerable_GetEnumerator_m2566823597_gshared (LinkedList_1_t1397221934 * __this, const MethodInfo* method)
{
	{
		NullCheck((LinkedList_1_t1397221934 *)__this);
		Enumerator_t2880376494  L_0 = ((  Enumerator_t2880376494  (*) (LinkedList_1_t1397221934 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4)->methodPointer)((LinkedList_1_t1397221934 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 4));
		Enumerator_t2880376494  L_1 = L_0;
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 5), &L_1);
		return (Il2CppObject *)L_2;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C"  bool LinkedList_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m3153905641_gshared (LinkedList_1_t1397221934 * __this, const MethodInfo* method)
{
	{
		return (bool)0;
	}
}
// System.Object System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManagerImpl/IdPair>::System.Collections.ICollection.get_SyncRoot()
extern "C"  Il2CppObject * LinkedList_1_System_Collections_ICollection_get_SyncRoot_m2237406132_gshared (LinkedList_1_t1397221934 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_syncRoot_2();
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManagerImpl/IdPair>::VerifyReferencedNode(System.Collections.Generic.LinkedListNode`1<T>)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3386882;
extern const uint32_t LinkedList_1_VerifyReferencedNode_m2661822725_MetadataUsageId;
extern "C"  void LinkedList_1_VerifyReferencedNode_m2661822725_gshared (LinkedList_1_t1397221934 * __this, LinkedListNode_1_t3139321472 * ___node0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_VerifyReferencedNode_m2661822725_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LinkedListNode_1_t3139321472 * L_0 = ___node0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral3386882, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		LinkedListNode_1_t3139321472 * L_2 = ___node0;
		NullCheck((LinkedListNode_1_t3139321472 *)L_2);
		LinkedList_1_t1397221934 * L_3 = ((  LinkedList_1_t1397221934 * (*) (LinkedListNode_1_t3139321472 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6)->methodPointer)((LinkedListNode_1_t3139321472 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 6));
		if ((((Il2CppObject*)(LinkedList_1_t1397221934 *)L_3) == ((Il2CppObject*)(LinkedList_1_t1397221934 *)__this)))
		{
			goto IL_0023;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_4 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_4);
	}

IL_0023:
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManagerImpl/IdPair>::AddLast(T)
extern "C"  LinkedListNode_1_t3139321472 * LinkedList_1_AddLast_m3506275859_gshared (LinkedList_1_t1397221934 * __this, IdPair_t3522586765  ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t3139321472 * V_0 = NULL;
	{
		LinkedListNode_1_t3139321472 * L_0 = (LinkedListNode_1_t3139321472 *)__this->get_first_3();
		if (L_0)
		{
			goto IL_001f;
		}
	}
	{
		IdPair_t3522586765  L_1 = ___value0;
		LinkedListNode_1_t3139321472 * L_2 = (LinkedListNode_1_t3139321472 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (LinkedListNode_1_t3139321472 *, LinkedList_1_t1397221934 *, IdPair_t3522586765 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8)->methodPointer)(L_2, (LinkedList_1_t1397221934 *)__this, (IdPair_t3522586765 )L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 8));
		V_0 = (LinkedListNode_1_t3139321472 *)L_2;
		LinkedListNode_1_t3139321472 * L_3 = V_0;
		__this->set_first_3(L_3);
		goto IL_0038;
	}

IL_001f:
	{
		IdPair_t3522586765  L_4 = ___value0;
		LinkedListNode_1_t3139321472 * L_5 = (LinkedListNode_1_t3139321472 *)__this->get_first_3();
		NullCheck(L_5);
		LinkedListNode_1_t3139321472 * L_6 = (LinkedListNode_1_t3139321472 *)L_5->get_back_3();
		LinkedListNode_1_t3139321472 * L_7 = (LinkedListNode_1_t3139321472 *)__this->get_first_3();
		LinkedListNode_1_t3139321472 * L_8 = (LinkedListNode_1_t3139321472 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 7));
		((  void (*) (LinkedListNode_1_t3139321472 *, LinkedList_1_t1397221934 *, IdPair_t3522586765 , LinkedListNode_1_t3139321472 *, LinkedListNode_1_t3139321472 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9)->methodPointer)(L_8, (LinkedList_1_t1397221934 *)__this, (IdPair_t3522586765 )L_4, (LinkedListNode_1_t3139321472 *)L_6, (LinkedListNode_1_t3139321472 *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 9));
		V_0 = (LinkedListNode_1_t3139321472 *)L_8;
	}

IL_0038:
	{
		uint32_t L_9 = (uint32_t)__this->get_count_0();
		__this->set_count_0(((int32_t)((int32_t)L_9+(int32_t)1)));
		uint32_t L_10 = (uint32_t)__this->get_version_1();
		__this->set_version_1(((int32_t)((int32_t)L_10+(int32_t)1)));
		LinkedListNode_1_t3139321472 * L_11 = V_0;
		return L_11;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManagerImpl/IdPair>::Clear()
extern "C"  void LinkedList_1_Clear_m1123747601_gshared (LinkedList_1_t1397221934 * __this, const MethodInfo* method)
{
	{
		goto IL_000b;
	}

IL_0005:
	{
		NullCheck((LinkedList_1_t1397221934 *)__this);
		((  void (*) (LinkedList_1_t1397221934 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10)->methodPointer)((LinkedList_1_t1397221934 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 10));
	}

IL_000b:
	{
		LinkedListNode_1_t3139321472 * L_0 = (LinkedListNode_1_t3139321472 *)__this->get_first_3();
		if (L_0)
		{
			goto IL_0005;
		}
	}
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManagerImpl/IdPair>::Contains(T)
extern "C"  bool LinkedList_1_Contains_m3144929522_gshared (LinkedList_1_t1397221934 * __this, IdPair_t3522586765  ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t3139321472 * V_0 = NULL;
	{
		LinkedListNode_1_t3139321472 * L_0 = (LinkedListNode_1_t3139321472 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t3139321472 *)L_0;
		LinkedListNode_1_t3139321472 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}

IL_000f:
	{
		LinkedListNode_1_t3139321472 * L_2 = V_0;
		NullCheck((LinkedListNode_1_t3139321472 *)L_2);
		IdPair_t3522586765  L_3 = ((  IdPair_t3522586765  (*) (LinkedListNode_1_t3139321472 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t3139321472 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		IdPair_t3522586765  L_4 = L_3;
		Il2CppObject * L_5 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 12), &L_4);
		Il2CppObject * L_6 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 12), (&___value0));
		NullCheck((Il2CppObject *)L_6);
		bool L_7 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_6, (Il2CppObject *)L_5);
		if (!L_7)
		{
			goto IL_002e;
		}
	}
	{
		return (bool)1;
	}

IL_002e:
	{
		LinkedListNode_1_t3139321472 * L_8 = V_0;
		NullCheck(L_8);
		LinkedListNode_1_t3139321472 * L_9 = (LinkedListNode_1_t3139321472 *)L_8->get_forward_2();
		V_0 = (LinkedListNode_1_t3139321472 *)L_9;
		LinkedListNode_1_t3139321472 * L_10 = V_0;
		LinkedListNode_1_t3139321472 * L_11 = (LinkedListNode_1_t3139321472 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t3139321472 *)L_10) == ((Il2CppObject*)(LinkedListNode_1_t3139321472 *)L_11))))
		{
			goto IL_000f;
		}
	}
	{
		return (bool)0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManagerImpl/IdPair>::CopyTo(T[],System.Int32)
extern Il2CppClass* ArgumentNullException_t3573189601_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var;
extern Il2CppClass* ArgumentException_t928607144_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93090393;
extern Il2CppCodeGenString* _stringLiteral100346066;
extern Il2CppCodeGenString* _stringLiteral279574791;
extern Il2CppCodeGenString* _stringLiteral768919341;
extern const uint32_t LinkedList_1_CopyTo_m429622409_MetadataUsageId;
extern "C"  void LinkedList_1_CopyTo_m429622409_gshared (LinkedList_1_t1397221934 * __this, IdPairU5BU5D_t296135328* ___array0, int32_t ___index1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_CopyTo_m429622409_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	LinkedListNode_1_t3139321472 * V_0 = NULL;
	{
		IdPairU5BU5D_t296135328* L_0 = ___array0;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t3573189601 * L_1 = (ArgumentNullException_t3573189601 *)il2cpp_codegen_object_new(ArgumentNullException_t3573189601_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m135444188(L_1, (String_t*)_stringLiteral93090393, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0011:
	{
		int32_t L_2 = ___index1;
		IdPairU5BU5D_t296135328* L_3 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_3);
		int32_t L_4 = Array_GetLowerBound_m2369136542((Il2CppArray *)(Il2CppArray *)L_3, (int32_t)0, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_2) < ((uint32_t)L_4))))
		{
			goto IL_0029;
		}
	}
	{
		ArgumentOutOfRangeException_t3816648464 * L_5 = (ArgumentOutOfRangeException_t3816648464 *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t3816648464_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m2026296331(L_5, (String_t*)_stringLiteral100346066, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0029:
	{
		IdPairU5BU5D_t296135328* L_6 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_6);
		int32_t L_7 = Array_get_Rank_m1671008509((Il2CppArray *)(Il2CppArray *)L_6, /*hidden argument*/NULL);
		if ((((int32_t)L_7) == ((int32_t)1)))
		{
			goto IL_0045;
		}
	}
	{
		ArgumentException_t928607144 * L_8 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m732321503(L_8, (String_t*)_stringLiteral93090393, (String_t*)_stringLiteral279574791, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_8);
	}

IL_0045:
	{
		IdPairU5BU5D_t296135328* L_9 = ___array0;
		NullCheck(L_9);
		int32_t L_10 = ___index1;
		IdPairU5BU5D_t296135328* L_11 = ___array0;
		NullCheck((Il2CppArray *)(Il2CppArray *)L_11);
		int32_t L_12 = Array_GetLowerBound_m2369136542((Il2CppArray *)(Il2CppArray *)L_11, (int32_t)0, /*hidden argument*/NULL);
		uint32_t L_13 = (uint32_t)__this->get_count_0();
		if ((((int64_t)(((int64_t)((int64_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_9)->max_length))))-(int32_t)L_10))+(int32_t)L_12)))))) >= ((int64_t)(((int64_t)((uint64_t)L_13))))))
		{
			goto IL_006a;
		}
	}
	{
		ArgumentException_t928607144 * L_14 = (ArgumentException_t928607144 *)il2cpp_codegen_object_new(ArgumentException_t928607144_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m3544856547(L_14, (String_t*)_stringLiteral768919341, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_14);
	}

IL_006a:
	{
		LinkedListNode_1_t3139321472 * L_15 = (LinkedListNode_1_t3139321472 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t3139321472 *)L_15;
		LinkedListNode_1_t3139321472 * L_16 = (LinkedListNode_1_t3139321472 *)__this->get_first_3();
		if (L_16)
		{
			goto IL_007d;
		}
	}
	{
		return;
	}

IL_007d:
	{
		IdPairU5BU5D_t296135328* L_17 = ___array0;
		int32_t L_18 = ___index1;
		LinkedListNode_1_t3139321472 * L_19 = V_0;
		NullCheck((LinkedListNode_1_t3139321472 *)L_19);
		IdPair_t3522586765  L_20 = ((  IdPair_t3522586765  (*) (LinkedListNode_1_t3139321472 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t3139321472 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		(L_17)->SetAt(static_cast<il2cpp_array_size_t>(L_18), (IdPair_t3522586765 )L_20);
		int32_t L_21 = ___index1;
		___index1 = (int32_t)((int32_t)((int32_t)L_21+(int32_t)1));
		LinkedListNode_1_t3139321472 * L_22 = V_0;
		NullCheck(L_22);
		LinkedListNode_1_t3139321472 * L_23 = (LinkedListNode_1_t3139321472 *)L_22->get_forward_2();
		V_0 = (LinkedListNode_1_t3139321472 *)L_23;
		LinkedListNode_1_t3139321472 * L_24 = V_0;
		LinkedListNode_1_t3139321472 * L_25 = (LinkedListNode_1_t3139321472 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t3139321472 *)L_24) == ((Il2CppObject*)(LinkedListNode_1_t3139321472 *)L_25))))
		{
			goto IL_007d;
		}
	}
	{
		return;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManagerImpl/IdPair>::Find(T)
extern "C"  LinkedListNode_1_t3139321472 * LinkedList_1_Find_m2375202261_gshared (LinkedList_1_t1397221934 * __this, IdPair_t3522586765  ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t3139321472 * V_0 = NULL;
	{
		LinkedListNode_1_t3139321472 * L_0 = (LinkedListNode_1_t3139321472 *)__this->get_first_3();
		V_0 = (LinkedListNode_1_t3139321472 *)L_0;
		LinkedListNode_1_t3139321472 * L_1 = V_0;
		if (L_1)
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t3139321472 *)NULL;
	}

IL_000f:
	{
		goto IL_002a;
	}
	{
		LinkedListNode_1_t3139321472 * L_3 = V_0;
		NullCheck((LinkedListNode_1_t3139321472 *)L_3);
		IdPair_t3522586765  L_4 = ((  IdPair_t3522586765  (*) (LinkedListNode_1_t3139321472 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t3139321472 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
	}

IL_002a:
	{
	}
	{
		LinkedListNode_1_t3139321472 * L_6 = V_0;
		NullCheck((LinkedListNode_1_t3139321472 *)L_6);
		IdPair_t3522586765  L_7 = ((  IdPair_t3522586765  (*) (LinkedListNode_1_t3139321472 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11)->methodPointer)((LinkedListNode_1_t3139321472 *)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 11));
		IdPair_t3522586765  L_8 = L_7;
		Il2CppObject * L_9 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 12), &L_8);
		Il2CppObject * L_10 = Box(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 12), (&___value0));
		NullCheck((Il2CppObject *)L_10);
		bool L_11 = VirtFuncInvoker1< bool, Il2CppObject * >::Invoke(0 /* System.Boolean System.Object::Equals(System.Object) */, (Il2CppObject *)L_10, (Il2CppObject *)L_9);
		if (!L_11)
		{
			goto IL_0054;
		}
	}

IL_0052:
	{
		LinkedListNode_1_t3139321472 * L_12 = V_0;
		return L_12;
	}

IL_0054:
	{
		LinkedListNode_1_t3139321472 * L_13 = V_0;
		NullCheck(L_13);
		LinkedListNode_1_t3139321472 * L_14 = (LinkedListNode_1_t3139321472 *)L_13->get_forward_2();
		V_0 = (LinkedListNode_1_t3139321472 *)L_14;
		LinkedListNode_1_t3139321472 * L_15 = V_0;
		LinkedListNode_1_t3139321472 * L_16 = (LinkedListNode_1_t3139321472 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t3139321472 *)L_15) == ((Il2CppObject*)(LinkedListNode_1_t3139321472 *)L_16))))
		{
			goto IL_000f;
		}
	}
	{
		return (LinkedListNode_1_t3139321472 *)NULL;
	}
}
// System.Collections.Generic.LinkedList`1/Enumerator<T> System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManagerImpl/IdPair>::GetEnumerator()
extern "C"  Enumerator_t2880376494  LinkedList_1_GetEnumerator_m502552000_gshared (LinkedList_1_t1397221934 * __this, const MethodInfo* method)
{
	{
		Enumerator_t2880376494  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Enumerator__ctor_m4100598954(&L_0, (LinkedList_1_t1397221934 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 13));
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManagerImpl/IdPair>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1612836015;
extern Il2CppCodeGenString* _stringLiteral351608024;
extern const uint32_t LinkedList_1_GetObjectData_m2330417028_MetadataUsageId;
extern "C"  void LinkedList_1_GetObjectData_m2330417028_gshared (LinkedList_1_t1397221934 * __this, SerializationInfo_t2185721892 * ___info0, StreamingContext_t2761351129  ___context1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_GetObjectData_m2330417028_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IdPairU5BU5D_t296135328* V_0 = NULL;
	{
		uint32_t L_0 = (uint32_t)__this->get_count_0();
		V_0 = (IdPairU5BU5D_t296135328*)((IdPairU5BU5D_t296135328*)SZArrayNew(IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 14), (uint32_t)(((uintptr_t)L_0))));
		IdPairU5BU5D_t296135328* L_1 = V_0;
		NullCheck((LinkedList_1_t1397221934 *)__this);
		((  void (*) (LinkedList_1_t1397221934 *, IdPairU5BU5D_t296135328*, int32_t, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3)->methodPointer)((LinkedList_1_t1397221934 *)__this, (IdPairU5BU5D_t296135328*)L_1, (int32_t)0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 3));
		SerializationInfo_t2185721892 * L_2 = ___info0;
		IdPairU5BU5D_t296135328* L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_4 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t2185721892 *)L_2);
		SerializationInfo_AddValue_m3341936982((SerializationInfo_t2185721892 *)L_2, (String_t*)_stringLiteral1612836015, (Il2CppObject *)(Il2CppObject *)L_3, (Type_t *)L_4, /*hidden argument*/NULL);
		SerializationInfo_t2185721892 * L_5 = ___info0;
		uint32_t L_6 = (uint32_t)__this->get_version_1();
		NullCheck((SerializationInfo_t2185721892 *)L_5);
		SerializationInfo_AddValue_m787539465((SerializationInfo_t2185721892 *)L_5, (String_t*)_stringLiteral351608024, (uint32_t)L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManagerImpl/IdPair>::OnDeserialization(System.Object)
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1612836015;
extern Il2CppCodeGenString* _stringLiteral351608024;
extern const uint32_t LinkedList_1_OnDeserialization_m2390066528_MetadataUsageId;
extern "C"  void LinkedList_1_OnDeserialization_m2390066528_gshared (LinkedList_1_t1397221934 * __this, Il2CppObject * ___sender0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LinkedList_1_OnDeserialization_m2390066528_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	IdPairU5BU5D_t296135328* V_0 = NULL;
	IdPair_t3522586765  V_1;
	memset(&V_1, 0, sizeof(V_1));
	IdPairU5BU5D_t296135328* V_2 = NULL;
	int32_t V_3 = 0;
	{
		SerializationInfo_t2185721892 * L_0 = (SerializationInfo_t2185721892 *)__this->get_si_4();
		if (!L_0)
		{
			goto IL_0074;
		}
	}
	{
		SerializationInfo_t2185721892 * L_1 = (SerializationInfo_t2185721892 *)__this->get_si_4();
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_2 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, (RuntimeTypeHandle_t2669177232 )LoadTypeToken(IL2CPP_RGCTX_TYPE(method->declaring_type->rgctx_data, 15)), /*hidden argument*/NULL);
		NullCheck((SerializationInfo_t2185721892 *)L_1);
		Il2CppObject * L_3 = SerializationInfo_GetValue_m4125471336((SerializationInfo_t2185721892 *)L_1, (String_t*)_stringLiteral1612836015, (Type_t *)L_2, /*hidden argument*/NULL);
		V_0 = (IdPairU5BU5D_t296135328*)((IdPairU5BU5D_t296135328*)Castclass(L_3, IL2CPP_RGCTX_DATA(method->declaring_type->rgctx_data, 2)));
		IdPairU5BU5D_t296135328* L_4 = V_0;
		if (!L_4)
		{
			goto IL_0057;
		}
	}
	{
		IdPairU5BU5D_t296135328* L_5 = V_0;
		V_2 = (IdPairU5BU5D_t296135328*)L_5;
		V_3 = (int32_t)0;
		goto IL_004e;
	}

IL_003a:
	{
		IdPairU5BU5D_t296135328* L_6 = V_2;
		int32_t L_7 = V_3;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		IdPair_t3522586765  L_9 = (L_6)->GetAt(static_cast<il2cpp_array_size_t>(L_8));
		V_1 = (IdPair_t3522586765 )L_9;
		IdPair_t3522586765  L_10 = V_1;
		NullCheck((LinkedList_1_t1397221934 *)__this);
		((  LinkedListNode_1_t3139321472 * (*) (LinkedList_1_t1397221934 *, IdPair_t3522586765 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1)->methodPointer)((LinkedList_1_t1397221934 *)__this, (IdPair_t3522586765 )L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 1));
		int32_t L_11 = V_3;
		V_3 = (int32_t)((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_12 = V_3;
		IdPairU5BU5D_t296135328* L_13 = V_2;
		NullCheck(L_13);
		if ((((int32_t)L_12) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))))
		{
			goto IL_003a;
		}
	}

IL_0057:
	{
		SerializationInfo_t2185721892 * L_14 = (SerializationInfo_t2185721892 *)__this->get_si_4();
		NullCheck((SerializationInfo_t2185721892 *)L_14);
		uint32_t L_15 = SerializationInfo_GetUInt32_m1908270281((SerializationInfo_t2185721892 *)L_14, (String_t*)_stringLiteral351608024, /*hidden argument*/NULL);
		__this->set_version_1(L_15);
		__this->set_si_4((SerializationInfo_t2185721892 *)NULL);
	}

IL_0074:
	{
		return;
	}
}
// System.Boolean System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManagerImpl/IdPair>::Remove(T)
extern "C"  bool LinkedList_1_Remove_m2926152983_gshared (LinkedList_1_t1397221934 * __this, IdPair_t3522586765  ___value0, const MethodInfo* method)
{
	LinkedListNode_1_t3139321472 * V_0 = NULL;
	{
		IdPair_t3522586765  L_0 = ___value0;
		NullCheck((LinkedList_1_t1397221934 *)__this);
		LinkedListNode_1_t3139321472 * L_1 = ((  LinkedListNode_1_t3139321472 * (*) (LinkedList_1_t1397221934 *, IdPair_t3522586765 , const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16)->methodPointer)((LinkedList_1_t1397221934 *)__this, (IdPair_t3522586765 )L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 16));
		V_0 = (LinkedListNode_1_t3139321472 *)L_1;
		LinkedListNode_1_t3139321472 * L_2 = V_0;
		if (L_2)
		{
			goto IL_0010;
		}
	}
	{
		return (bool)0;
	}

IL_0010:
	{
		LinkedListNode_1_t3139321472 * L_3 = V_0;
		NullCheck((LinkedList_1_t1397221934 *)__this);
		((  void (*) (LinkedList_1_t1397221934 *, LinkedListNode_1_t3139321472 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((LinkedList_1_t1397221934 *)__this, (LinkedListNode_1_t3139321472 *)L_3, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
		return (bool)1;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManagerImpl/IdPair>::Remove(System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedList_1_Remove_m4225422514_gshared (LinkedList_1_t1397221934 * __this, LinkedListNode_1_t3139321472 * ___node0, const MethodInfo* method)
{
	{
		LinkedListNode_1_t3139321472 * L_0 = ___node0;
		NullCheck((LinkedList_1_t1397221934 *)__this);
		((  void (*) (LinkedList_1_t1397221934 *, LinkedListNode_1_t3139321472 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18)->methodPointer)((LinkedList_1_t1397221934 *)__this, (LinkedListNode_1_t3139321472 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 18));
		uint32_t L_1 = (uint32_t)__this->get_count_0();
		__this->set_count_0(((int32_t)((int32_t)L_1-(int32_t)1)));
		uint32_t L_2 = (uint32_t)__this->get_count_0();
		if (L_2)
		{
			goto IL_0027;
		}
	}
	{
		__this->set_first_3((LinkedListNode_1_t3139321472 *)NULL);
	}

IL_0027:
	{
		LinkedListNode_1_t3139321472 * L_3 = ___node0;
		LinkedListNode_1_t3139321472 * L_4 = (LinkedListNode_1_t3139321472 *)__this->get_first_3();
		if ((!(((Il2CppObject*)(LinkedListNode_1_t3139321472 *)L_3) == ((Il2CppObject*)(LinkedListNode_1_t3139321472 *)L_4))))
		{
			goto IL_0044;
		}
	}
	{
		LinkedListNode_1_t3139321472 * L_5 = (LinkedListNode_1_t3139321472 *)__this->get_first_3();
		NullCheck(L_5);
		LinkedListNode_1_t3139321472 * L_6 = (LinkedListNode_1_t3139321472 *)L_5->get_forward_2();
		__this->set_first_3(L_6);
	}

IL_0044:
	{
		uint32_t L_7 = (uint32_t)__this->get_version_1();
		__this->set_version_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		LinkedListNode_1_t3139321472 * L_8 = ___node0;
		NullCheck((LinkedListNode_1_t3139321472 *)L_8);
		((  void (*) (LinkedListNode_1_t3139321472 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19)->methodPointer)((LinkedListNode_1_t3139321472 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 19));
		return;
	}
}
// System.Void System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManagerImpl/IdPair>::RemoveLast()
extern "C"  void LinkedList_1_RemoveLast_m195852952_gshared (LinkedList_1_t1397221934 * __this, const MethodInfo* method)
{
	{
		LinkedListNode_1_t3139321472 * L_0 = (LinkedListNode_1_t3139321472 *)__this->get_first_3();
		if (!L_0)
		{
			goto IL_001c;
		}
	}
	{
		LinkedListNode_1_t3139321472 * L_1 = (LinkedListNode_1_t3139321472 *)__this->get_first_3();
		NullCheck(L_1);
		LinkedListNode_1_t3139321472 * L_2 = (LinkedListNode_1_t3139321472 *)L_1->get_back_3();
		NullCheck((LinkedList_1_t1397221934 *)__this);
		((  void (*) (LinkedList_1_t1397221934 *, LinkedListNode_1_t3139321472 *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17)->methodPointer)((LinkedList_1_t1397221934 *)__this, (LinkedListNode_1_t3139321472 *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->declaring_type->rgctx_data, 17));
	}

IL_001c:
	{
		return;
	}
}
// System.Int32 System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManagerImpl/IdPair>::get_Count()
extern "C"  int32_t LinkedList_1_get_Count_m640713804_gshared (LinkedList_1_t1397221934 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = (uint32_t)__this->get_count_0();
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1<Vuforia.VuforiaManagerImpl/IdPair>::get_First()
extern "C"  LinkedListNode_1_t3139321472 * LinkedList_1_get_First_m870657490_gshared (LinkedList_1_t1397221934 * __this, const MethodInfo* method)
{
	{
		LinkedListNode_1_t3139321472 * L_0 = (LinkedListNode_1_t3139321472 *)__this->get_first_3();
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
extern "C"  void LinkedListNode_1__ctor_m648136130_gshared (LinkedListNode_1_t3787551078 * __this, LinkedList_1_t2045451540 * ___list0, Il2CppObject * ___value1, const MethodInfo* method)
{
	LinkedListNode_1_t3787551078 * V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		LinkedList_1_t2045451540 * L_0 = ___list0;
		__this->set_container_1(L_0);
		Il2CppObject * L_1 = ___value1;
		__this->set_item_0(L_1);
		V_0 = (LinkedListNode_1_t3787551078 *)__this;
		__this->set_forward_2(__this);
		LinkedListNode_1_t3787551078 * L_2 = V_0;
		__this->set_back_3(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedListNode_1__ctor_m448391458_gshared (LinkedListNode_1_t3787551078 * __this, LinkedList_1_t2045451540 * ___list0, Il2CppObject * ___value1, LinkedListNode_1_t3787551078 * ___previousNode2, LinkedListNode_1_t3787551078 * ___nextNode3, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		LinkedList_1_t2045451540 * L_0 = ___list0;
		__this->set_container_1(L_0);
		Il2CppObject * L_1 = ___value1;
		__this->set_item_0(L_1);
		LinkedListNode_1_t3787551078 * L_2 = ___previousNode2;
		__this->set_back_3(L_2);
		LinkedListNode_1_t3787551078 * L_3 = ___nextNode3;
		__this->set_forward_2(L_3);
		LinkedListNode_1_t3787551078 * L_4 = ___previousNode2;
		NullCheck(L_4);
		L_4->set_forward_2(__this);
		LinkedListNode_1_t3787551078 * L_5 = ___nextNode3;
		NullCheck(L_5);
		L_5->set_back_3(__this);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<System.Object>::Detach()
extern "C"  void LinkedListNode_1_Detach_m3406254942_gshared (LinkedListNode_1_t3787551078 * __this, const MethodInfo* method)
{
	LinkedListNode_1_t3787551078 * V_0 = NULL;
	{
		LinkedListNode_1_t3787551078 * L_0 = (LinkedListNode_1_t3787551078 *)__this->get_back_3();
		LinkedListNode_1_t3787551078 * L_1 = (LinkedListNode_1_t3787551078 *)__this->get_forward_2();
		NullCheck(L_0);
		L_0->set_forward_2(L_1);
		LinkedListNode_1_t3787551078 * L_2 = (LinkedListNode_1_t3787551078 *)__this->get_forward_2();
		LinkedListNode_1_t3787551078 * L_3 = (LinkedListNode_1_t3787551078 *)__this->get_back_3();
		NullCheck(L_2);
		L_2->set_back_3(L_3);
		V_0 = (LinkedListNode_1_t3787551078 *)NULL;
		__this->set_back_3((LinkedListNode_1_t3787551078 *)NULL);
		LinkedListNode_1_t3787551078 * L_4 = V_0;
		__this->set_forward_2(L_4);
		__this->set_container_1((LinkedList_1_t2045451540 *)NULL);
		return;
	}
}
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_List()
extern "C"  LinkedList_1_t2045451540 * LinkedListNode_1_get_List_m3467110818_gshared (LinkedListNode_1_t3787551078 * __this, const MethodInfo* method)
{
	{
		LinkedList_1_t2045451540 * L_0 = (LinkedList_1_t2045451540 *)__this->get_container_1();
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<System.Object>::get_Next()
extern "C"  LinkedListNode_1_t3787551078 * LinkedListNode_1_get_Next_m1427618777_gshared (LinkedListNode_1_t3787551078 * __this, const MethodInfo* method)
{
	LinkedListNode_1_t3787551078 * G_B4_0 = NULL;
	{
		LinkedList_1_t2045451540 * L_0 = (LinkedList_1_t2045451540 *)__this->get_container_1();
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		LinkedListNode_1_t3787551078 * L_1 = (LinkedListNode_1_t3787551078 *)__this->get_forward_2();
		LinkedList_1_t2045451540 * L_2 = (LinkedList_1_t2045451540 *)__this->get_container_1();
		NullCheck(L_2);
		LinkedListNode_1_t3787551078 * L_3 = (LinkedListNode_1_t3787551078 *)L_2->get_first_3();
		if ((((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_1) == ((Il2CppObject*)(LinkedListNode_1_t3787551078 *)L_3)))
		{
			goto IL_002c;
		}
	}
	{
		LinkedListNode_1_t3787551078 * L_4 = (LinkedListNode_1_t3787551078 *)__this->get_forward_2();
		G_B4_0 = L_4;
		goto IL_002d;
	}

IL_002c:
	{
		G_B4_0 = ((LinkedListNode_1_t3787551078 *)(NULL));
	}

IL_002d:
	{
		return G_B4_0;
	}
}
// T System.Collections.Generic.LinkedListNode`1<System.Object>::get_Value()
extern "C"  Il2CppObject * LinkedListNode_1_get_Value_m702633824_gshared (LinkedListNode_1_t3787551078 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_item_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<Vuforia.VuforiaManagerImpl/IdPair>::.ctor(System.Collections.Generic.LinkedList`1<T>,T)
extern "C"  void LinkedListNode_1__ctor_m1128828465_gshared (LinkedListNode_1_t3139321472 * __this, LinkedList_1_t1397221934 * ___list0, IdPair_t3522586765  ___value1, const MethodInfo* method)
{
	LinkedListNode_1_t3139321472 * V_0 = NULL;
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		LinkedList_1_t1397221934 * L_0 = ___list0;
		__this->set_container_1(L_0);
		IdPair_t3522586765  L_1 = ___value1;
		__this->set_item_0(L_1);
		V_0 = (LinkedListNode_1_t3139321472 *)__this;
		__this->set_forward_2(__this);
		LinkedListNode_1_t3139321472 * L_2 = V_0;
		__this->set_back_3(L_2);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<Vuforia.VuforiaManagerImpl/IdPair>::.ctor(System.Collections.Generic.LinkedList`1<T>,T,System.Collections.Generic.LinkedListNode`1<T>,System.Collections.Generic.LinkedListNode`1<T>)
extern "C"  void LinkedListNode_1__ctor_m965250385_gshared (LinkedListNode_1_t3139321472 * __this, LinkedList_1_t1397221934 * ___list0, IdPair_t3522586765  ___value1, LinkedListNode_1_t3139321472 * ___previousNode2, LinkedListNode_1_t3139321472 * ___nextNode3, const MethodInfo* method)
{
	{
		NullCheck((Il2CppObject *)__this);
		Object__ctor_m1772956182((Il2CppObject *)__this, /*hidden argument*/NULL);
		LinkedList_1_t1397221934 * L_0 = ___list0;
		__this->set_container_1(L_0);
		IdPair_t3522586765  L_1 = ___value1;
		__this->set_item_0(L_1);
		LinkedListNode_1_t3139321472 * L_2 = ___previousNode2;
		__this->set_back_3(L_2);
		LinkedListNode_1_t3139321472 * L_3 = ___nextNode3;
		__this->set_forward_2(L_3);
		LinkedListNode_1_t3139321472 * L_4 = ___previousNode2;
		NullCheck(L_4);
		L_4->set_forward_2(__this);
		LinkedListNode_1_t3139321472 * L_5 = ___nextNode3;
		NullCheck(L_5);
		L_5->set_back_3(__this);
		return;
	}
}
// System.Void System.Collections.Generic.LinkedListNode`1<Vuforia.VuforiaManagerImpl/IdPair>::Detach()
extern "C"  void LinkedListNode_1_Detach_m4109991503_gshared (LinkedListNode_1_t3139321472 * __this, const MethodInfo* method)
{
	LinkedListNode_1_t3139321472 * V_0 = NULL;
	{
		LinkedListNode_1_t3139321472 * L_0 = (LinkedListNode_1_t3139321472 *)__this->get_back_3();
		LinkedListNode_1_t3139321472 * L_1 = (LinkedListNode_1_t3139321472 *)__this->get_forward_2();
		NullCheck(L_0);
		L_0->set_forward_2(L_1);
		LinkedListNode_1_t3139321472 * L_2 = (LinkedListNode_1_t3139321472 *)__this->get_forward_2();
		LinkedListNode_1_t3139321472 * L_3 = (LinkedListNode_1_t3139321472 *)__this->get_back_3();
		NullCheck(L_2);
		L_2->set_back_3(L_3);
		V_0 = (LinkedListNode_1_t3139321472 *)NULL;
		__this->set_back_3((LinkedListNode_1_t3139321472 *)NULL);
		LinkedListNode_1_t3139321472 * L_4 = V_0;
		__this->set_forward_2(L_4);
		__this->set_container_1((LinkedList_1_t1397221934 *)NULL);
		return;
	}
}
// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1<Vuforia.VuforiaManagerImpl/IdPair>::get_List()
extern "C"  LinkedList_1_t1397221934 * LinkedListNode_1_get_List_m4017488979_gshared (LinkedListNode_1_t3139321472 * __this, const MethodInfo* method)
{
	{
		LinkedList_1_t1397221934 * L_0 = (LinkedList_1_t1397221934 *)__this->get_container_1();
		return L_0;
	}
}
// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1<Vuforia.VuforiaManagerImpl/IdPair>::get_Next()
extern "C"  LinkedListNode_1_t3139321472 * LinkedListNode_1_get_Next_m2854000145_gshared (LinkedListNode_1_t3139321472 * __this, const MethodInfo* method)
{
	LinkedListNode_1_t3139321472 * G_B4_0 = NULL;
	{
		LinkedList_1_t1397221934 * L_0 = (LinkedList_1_t1397221934 *)__this->get_container_1();
		if (!L_0)
		{
			goto IL_002c;
		}
	}
	{
		LinkedListNode_1_t3139321472 * L_1 = (LinkedListNode_1_t3139321472 *)__this->get_forward_2();
		LinkedList_1_t1397221934 * L_2 = (LinkedList_1_t1397221934 *)__this->get_container_1();
		NullCheck(L_2);
		LinkedListNode_1_t3139321472 * L_3 = (LinkedListNode_1_t3139321472 *)L_2->get_first_3();
		if ((((Il2CppObject*)(LinkedListNode_1_t3139321472 *)L_1) == ((Il2CppObject*)(LinkedListNode_1_t3139321472 *)L_3)))
		{
			goto IL_002c;
		}
	}
	{
		LinkedListNode_1_t3139321472 * L_4 = (LinkedListNode_1_t3139321472 *)__this->get_forward_2();
		G_B4_0 = L_4;
		goto IL_002d;
	}

IL_002c:
	{
		G_B4_0 = ((LinkedListNode_1_t3139321472 *)(NULL));
	}

IL_002d:
	{
		return G_B4_0;
	}
}
// T System.Collections.Generic.LinkedListNode`1<Vuforia.VuforiaManagerImpl/IdPair>::get_Value()
extern "C"  IdPair_t3522586765  LinkedListNode_1_get_Value_m1933611632_gshared (LinkedListNode_1_t3139321472 * __this, const MethodInfo* method)
{
	{
		IdPair_t3522586765  L_0 = (IdPair_t3522586765 )__this->get_item_0();
		return L_0;
	}
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1242988386_gshared (Enumerator_t2541696822 * __this, List_1_t2522024052 * ___l0, const MethodInfo* method)
{
	{
		List_1_t2522024052 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t2522024052 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1242988386_AdjustorThunk (Il2CppObject * __this, List_1_t2522024052 * ___l0, const MethodInfo* method)
{
	Enumerator_t2541696822 * _thisAdjusted = reinterpret_cast<Enumerator_t2541696822 *>(__this + 1);
	Enumerator__ctor_m1242988386(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1509621680_gshared (Enumerator_t2541696822 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t2541696822 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1509621680_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2541696822 * _thisAdjusted = reinterpret_cast<Enumerator_t2541696822 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1509621680(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m262228262_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m262228262_gshared (Enumerator_t2541696822 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m262228262_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t2541696822 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_current_3();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m262228262_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2541696822 * _thisAdjusted = reinterpret_cast<Enumerator_t2541696822 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m262228262(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
extern "C"  void Enumerator_Dispose_m3304555975_gshared (Enumerator_t2541696822 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t2522024052 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3304555975_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2541696822 * _thisAdjusted = reinterpret_cast<Enumerator_t2541696822 *>(__this + 1);
	Enumerator_Dispose_m3304555975(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m936708480_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m936708480_gshared (Enumerator_t2541696822 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m936708480_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t2522024052 * L_0 = (List_1_t2522024052 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t2541696822  L_1 = (*(Enumerator_t2541696822 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t2522024052 * L_7 = (List_1_t2522024052 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m936708480_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2541696822 * _thisAdjusted = reinterpret_cast<Enumerator_t2541696822 *>(__this + 1);
	Enumerator_VerifyState_m936708480(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1157355384_gshared (Enumerator_t2541696822 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t2541696822 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t2522024052 * L_2 = (List_1_t2522024052 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t2522024052 * L_4 = (List_1_t2522024052 *)__this->get_l_0();
		NullCheck(L_4);
		Int32U5BU5D_t3230847821* L_5 = (Int32U5BU5D_t3230847821*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1157355384_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2541696822 * _thisAdjusted = reinterpret_cast<Enumerator_t2541696822 *>(__this + 1);
	return Enumerator_MoveNext_m1157355384(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m1989858276_gshared (Enumerator_t2541696822 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return L_0;
	}
}
extern "C"  int32_t Enumerator_get_Current_m1989858276_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t2541696822 * _thisAdjusted = reinterpret_cast<Enumerator_t2541696822 *>(__this + 1);
	return Enumerator_get_Current_m1989858276(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m1029849669_gshared (Enumerator_t1263707397 * __this, List_1_t1244034627 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1244034627 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1244034627 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m1029849669_AdjustorThunk (Il2CppObject * __this, List_1_t1244034627 * ___l0, const MethodInfo* method)
{
	Enumerator_t1263707397 * _thisAdjusted = reinterpret_cast<Enumerator_t1263707397 *>(__this + 1);
	Enumerator__ctor_m1029849669(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m771996397_gshared (Enumerator_t1263707397 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1263707397 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m771996397_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1263707397 * _thisAdjusted = reinterpret_cast<Enumerator_t1263707397 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m771996397(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_gshared (Enumerator_t1263707397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1263707397 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		Il2CppObject * L_2 = (Il2CppObject *)__this->get_current_3();
		return L_2;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3561903705_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1263707397 * _thisAdjusted = reinterpret_cast<Enumerator_t1263707397 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3561903705(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C"  void Enumerator_Dispose_m2904289642_gshared (Enumerator_t1263707397 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1244034627 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2904289642_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1263707397 * _thisAdjusted = reinterpret_cast<Enumerator_t1263707397 *>(__this + 1);
	Enumerator_Dispose_m2904289642(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m1522854819_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1522854819_gshared (Enumerator_t1263707397 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1522854819_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1244034627 * L_0 = (List_1_t1244034627 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1263707397  L_1 = (*(Enumerator_t1263707397 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1244034627 * L_7 = (List_1_t1244034627 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1522854819_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1263707397 * _thisAdjusted = reinterpret_cast<Enumerator_t1263707397 *>(__this + 1);
	Enumerator_VerifyState_m1522854819(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m844464217_gshared (Enumerator_t1263707397 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1263707397 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1244034627 * L_2 = (List_1_t1244034627 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1244034627 * L_4 = (List_1_t1244034627 *)__this->get_l_0();
		NullCheck(L_4);
		ObjectU5BU5D_t1108656482* L_5 = (ObjectU5BU5D_t1108656482*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		Il2CppObject * L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m844464217_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1263707397 * _thisAdjusted = reinterpret_cast<Enumerator_t1263707397 *>(__this + 1);
	return Enumerator_MoveNext_m844464217(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C"  Il2CppObject * Enumerator_get_Current_m4198990746_gshared (Enumerator_t1263707397 * __this, const MethodInfo* method)
{
	{
		Il2CppObject * L_0 = (Il2CppObject *)__this->get_current_3();
		return L_0;
	}
}
extern "C"  Il2CppObject * Enumerator_get_Current_m4198990746_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1263707397 * _thisAdjusted = reinterpret_cast<Enumerator_t1263707397 *>(__this + 1);
	return Enumerator_get_Current_m4198990746(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m256124610_gshared (Enumerator_t152504015 * __this, List_1_t132831245 * ___l0, const MethodInfo* method)
{
	{
		List_1_t132831245 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t132831245 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m256124610_AdjustorThunk (Il2CppObject * __this, List_1_t132831245 * ___l0, const MethodInfo* method)
{
	Enumerator_t152504015 * _thisAdjusted = reinterpret_cast<Enumerator_t152504015 *>(__this + 1);
	Enumerator__ctor_m256124610(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4026154064_gshared (Enumerator_t152504015 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t152504015 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m4026154064_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t152504015 * _thisAdjusted = reinterpret_cast<Enumerator_t152504015 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m4026154064(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m143716486_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m143716486_gshared (Enumerator_t152504015 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m143716486_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t152504015 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		CustomAttributeNamedArgument_t3059612989  L_2 = (CustomAttributeNamedArgument_t3059612989 )__this->get_current_3();
		CustomAttributeNamedArgument_t3059612989  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m143716486_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t152504015 * _thisAdjusted = reinterpret_cast<Enumerator_t152504015 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m143716486(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C"  void Enumerator_Dispose_m855442727_gshared (Enumerator_t152504015 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t132831245 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m855442727_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t152504015 * _thisAdjusted = reinterpret_cast<Enumerator_t152504015 *>(__this + 1);
	Enumerator_Dispose_m855442727(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m508287200_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m508287200_gshared (Enumerator_t152504015 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m508287200_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t132831245 * L_0 = (List_1_t132831245 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t152504015  L_1 = (*(Enumerator_t152504015 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t132831245 * L_7 = (List_1_t132831245 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m508287200_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t152504015 * _thisAdjusted = reinterpret_cast<Enumerator_t152504015 *>(__this + 1);
	Enumerator_VerifyState_m508287200(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m3090636416_gshared (Enumerator_t152504015 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t152504015 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t132831245 * L_2 = (List_1_t132831245 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t132831245 * L_4 = (List_1_t132831245 *)__this->get_l_0();
		NullCheck(L_4);
		CustomAttributeNamedArgumentU5BU5D_t1983528240* L_5 = (CustomAttributeNamedArgumentU5BU5D_t1983528240*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		CustomAttributeNamedArgument_t3059612989  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m3090636416_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t152504015 * _thisAdjusted = reinterpret_cast<Enumerator_t152504015 *>(__this + 1);
	return Enumerator_MoveNext_m3090636416(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern "C"  CustomAttributeNamedArgument_t3059612989  Enumerator_get_Current_m473447609_gshared (Enumerator_t152504015 * __this, const MethodInfo* method)
{
	{
		CustomAttributeNamedArgument_t3059612989  L_0 = (CustomAttributeNamedArgument_t3059612989 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  CustomAttributeNamedArgument_t3059612989  Enumerator_get_Current_m473447609_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t152504015 * _thisAdjusted = reinterpret_cast<Enumerator_t152504015 *>(__this + 1);
	return Enumerator_get_Current_m473447609(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m444414259_gshared (Enumerator_t394184448 * __this, List_1_t374511678 * ___l0, const MethodInfo* method)
{
	{
		List_1_t374511678 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t374511678 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m444414259_AdjustorThunk (Il2CppObject * __this, List_1_t374511678 * ___l0, const MethodInfo* method)
{
	Enumerator_t394184448 * _thisAdjusted = reinterpret_cast<Enumerator_t394184448 *>(__this + 1);
	Enumerator__ctor_m444414259(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1403627327_gshared (Enumerator_t394184448 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t394184448 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1403627327_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t394184448 * _thisAdjusted = reinterpret_cast<Enumerator_t394184448 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1403627327(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1685728309_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1685728309_gshared (Enumerator_t394184448 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1685728309_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t394184448 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		CustomAttributeTypedArgument_t3301293422  L_2 = (CustomAttributeTypedArgument_t3301293422 )__this->get_current_3();
		CustomAttributeTypedArgument_t3301293422  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1685728309_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t394184448 * _thisAdjusted = reinterpret_cast<Enumerator_t394184448 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1685728309(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::Dispose()
extern "C"  void Enumerator_Dispose_m3403219928_gshared (Enumerator_t394184448 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t374511678 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m3403219928_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t394184448 * _thisAdjusted = reinterpret_cast<Enumerator_t394184448 *>(__this + 1);
	Enumerator_Dispose_m3403219928(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m1438062353_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1438062353_gshared (Enumerator_t394184448 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1438062353_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t374511678 * L_0 = (List_1_t374511678 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t394184448  L_1 = (*(Enumerator_t394184448 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t374511678 * L_7 = (List_1_t374511678 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1438062353_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t394184448 * _thisAdjusted = reinterpret_cast<Enumerator_t394184448 *>(__this + 1);
	Enumerator_VerifyState_m1438062353(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m467351023_gshared (Enumerator_t394184448 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t394184448 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t374511678 * L_2 = (List_1_t374511678 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t374511678 * L_4 = (List_1_t374511678 *)__this->get_l_0();
		NullCheck(L_4);
		CustomAttributeTypedArgumentU5BU5D_t2088020251* L_5 = (CustomAttributeTypedArgumentU5BU5D_t2088020251*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		CustomAttributeTypedArgument_t3301293422  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m467351023_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t394184448 * _thisAdjusted = reinterpret_cast<Enumerator_t394184448 *>(__this + 1);
	return Enumerator_MoveNext_m467351023(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeTypedArgument>::get_Current()
extern "C"  CustomAttributeTypedArgument_t3301293422  Enumerator_get_Current_m1403222762_gshared (Enumerator_t394184448 * __this, const MethodInfo* method)
{
	{
		CustomAttributeTypedArgument_t3301293422  L_0 = (CustomAttributeTypedArgument_t3301293422 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  CustomAttributeTypedArgument_t3301293422  Enumerator_get_Current_m1403222762_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t394184448 * _thisAdjusted = reinterpret_cast<Enumerator_t394184448 *>(__this + 1);
	return Enumerator_get_Current_m1403222762(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.CameraDevice/CameraField>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m2176291008_gshared (Enumerator_t4262422655 * __this, List_1_t4242749885 * ___l0, const MethodInfo* method)
{
	{
		List_1_t4242749885 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t4242749885 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m2176291008_AdjustorThunk (Il2CppObject * __this, List_1_t4242749885 * ___l0, const MethodInfo* method)
{
	Enumerator_t4262422655 * _thisAdjusted = reinterpret_cast<Enumerator_t4262422655 *>(__this + 1);
	Enumerator__ctor_m2176291008(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.CameraDevice/CameraField>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1785727890_gshared (Enumerator_t4262422655 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t4262422655 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m1785727890_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4262422655 * _thisAdjusted = reinterpret_cast<Enumerator_t4262422655 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m1785727890(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.CameraDevice/CameraField>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m3055163144_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3055163144_gshared (Enumerator_t4262422655 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m3055163144_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t4262422655 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		CameraField_t2874564333  L_2 = (CameraField_t2874564333 )__this->get_current_3();
		CameraField_t2874564333  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m3055163144_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4262422655 * _thisAdjusted = reinterpret_cast<Enumerator_t4262422655 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m3055163144(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.CameraDevice/CameraField>::Dispose()
extern "C"  void Enumerator_Dispose_m2916372133_gshared (Enumerator_t4262422655 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t4242749885 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m2916372133_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4262422655 * _thisAdjusted = reinterpret_cast<Enumerator_t4262422655 *>(__this + 1);
	Enumerator_Dispose_m2916372133(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.CameraDevice/CameraField>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m1631990622_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1631990622_gshared (Enumerator_t4262422655 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1631990622_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t4242749885 * L_0 = (List_1_t4242749885 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t4262422655  L_1 = (*(Enumerator_t4262422655 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t4242749885 * L_7 = (List_1_t4242749885 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1631990622_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4262422655 * _thisAdjusted = reinterpret_cast<Enumerator_t4262422655 *>(__this + 1);
	Enumerator_VerifyState_m1631990622(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.CameraDevice/CameraField>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m1607553090_gshared (Enumerator_t4262422655 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t4262422655 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t4242749885 * L_2 = (List_1_t4242749885 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t4242749885 * L_4 = (List_1_t4242749885 *)__this->get_l_0();
		NullCheck(L_4);
		CameraFieldU5BU5D_t3664195264* L_5 = (CameraFieldU5BU5D_t3664195264*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		CameraField_t2874564333  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m1607553090_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4262422655 * _thisAdjusted = reinterpret_cast<Enumerator_t4262422655 *>(__this + 1);
	return Enumerator_MoveNext_m1607553090(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<Vuforia.CameraDevice/CameraField>::get_Current()
extern "C"  CameraField_t2874564333  Enumerator_get_Current_m1129219319_gshared (Enumerator_t4262422655 * __this, const MethodInfo* method)
{
	{
		CameraField_t2874564333  L_0 = (CameraField_t2874564333 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  CameraField_t2874564333  Enumerator_get_Current_m1129219319_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t4262422655 * _thisAdjusted = reinterpret_cast<Enumerator_t4262422655 *>(__this + 1);
	return Enumerator_get_Current_m1129219319(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m704684707_gshared (Enumerator_t1742233378 * __this, List_1_t1722560608 * ___l0, const MethodInfo* method)
{
	{
		List_1_t1722560608 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t1722560608 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m704684707_AdjustorThunk (Il2CppObject * __this, List_1_t1722560608 * ___l0, const MethodInfo* method)
{
	Enumerator_t1742233378 * _thisAdjusted = reinterpret_cast<Enumerator_t1742233378 *>(__this + 1);
	Enumerator__ctor_m704684707(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2250643407_gshared (Enumerator_t1742233378 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1742233378 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2250643407_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1742233378 * _thisAdjusted = reinterpret_cast<Enumerator_t1742233378 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2250643407(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m1051483461_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1051483461_gshared (Enumerator_t1742233378 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m1051483461_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1742233378 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		int32_t L_2 = (int32_t)__this->get_current_3();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m1051483461_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1742233378 * _thisAdjusted = reinterpret_cast<Enumerator_t1742233378 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m1051483461(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::Dispose()
extern "C"  void Enumerator_Dispose_m1475987784_gshared (Enumerator_t1742233378 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t1722560608 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1475987784_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1742233378 * _thisAdjusted = reinterpret_cast<Enumerator_t1742233378 *>(__this + 1);
	Enumerator_Dispose_m1475987784(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m823633025_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m823633025_gshared (Enumerator_t1742233378 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m823633025_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t1722560608 * L_0 = (List_1_t1722560608 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t1742233378  L_1 = (*(Enumerator_t1742233378 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t1722560608 * L_7 = (List_1_t1722560608 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m823633025_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1742233378 * _thisAdjusted = reinterpret_cast<Enumerator_t1742233378 *>(__this + 1);
	Enumerator_VerifyState_m823633025(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m2093101951_gshared (Enumerator_t1742233378 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t1742233378 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t1722560608 * L_2 = (List_1_t1722560608 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t1722560608 * L_4 = (List_1_t1722560608 *)__this->get_l_0();
		NullCheck(L_4);
		PIXEL_FORMATU5BU5D_t882070065* L_5 = (PIXEL_FORMATU5BU5D_t882070065*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		int32_t L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m2093101951_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1742233378 * _thisAdjusted = reinterpret_cast<Enumerator_t1742233378 *>(__this + 1);
	return Enumerator_MoveNext_m2093101951(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<Vuforia.Image/PIXEL_FORMAT>::get_Current()
extern "C"  int32_t Enumerator_get_Current_m2537161434_gshared (Enumerator_t1742233378 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (int32_t)__this->get_current_3();
		return L_0;
	}
}
extern "C"  int32_t Enumerator_get_Current_m2537161434_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t1742233378 * _thisAdjusted = reinterpret_cast<Enumerator_t1742233378 *>(__this + 1);
	return Enumerator_get_Current_m2537161434(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::.ctor(System.Collections.Generic.List`1<T>)
extern "C"  void Enumerator__ctor_m3526812969_gshared (Enumerator_t702301140 * __this, List_1_t682628370 * ___l0, const MethodInfo* method)
{
	{
		List_1_t682628370 * L_0 = ___l0;
		__this->set_l_0(L_0);
		List_1_t682628370 * L_1 = ___l0;
		NullCheck(L_1);
		int32_t L_2 = (int32_t)L_1->get__version_3();
		__this->set_ver_2(L_2);
		return;
	}
}
extern "C"  void Enumerator__ctor_m3526812969_AdjustorThunk (Il2CppObject * __this, List_1_t682628370 * ___l0, const MethodInfo* method)
{
	Enumerator_t702301140 * _thisAdjusted = reinterpret_cast<Enumerator_t702301140 *>(__this + 1);
	Enumerator__ctor_m3526812969(_thisAdjusted, ___l0, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerator.Reset()
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2248530313_gshared (Enumerator_t702301140 * __this, const MethodInfo* method)
{
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t702301140 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		__this->set_next_1(0);
		return;
	}
}
extern "C"  void Enumerator_System_Collections_IEnumerator_Reset_m2248530313_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t702301140 * _thisAdjusted = reinterpret_cast<Enumerator_t702301140 *>(__this + 1);
	Enumerator_System_Collections_IEnumerator_Reset_m2248530313(_thisAdjusted, method);
}
// System.Object System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::System.Collections.IEnumerator.get_Current()
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern const uint32_t Enumerator_System_Collections_IEnumerator_get_Current_m541630517_MetadataUsageId;
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m541630517_gshared (Enumerator_t702301140 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_System_Collections_IEnumerator_get_Current_m541630517_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t702301140 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0018;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_1 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m355676978(L_1, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1);
	}

IL_0018:
	{
		TargetSearchResult_t3609410114  L_2 = (TargetSearchResult_t3609410114 )__this->get_current_3();
		TargetSearchResult_t3609410114  L_3 = L_2;
		Il2CppObject * L_4 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 1), &L_3);
		return L_4;
	}
}
extern "C"  Il2CppObject * Enumerator_System_Collections_IEnumerator_get_Current_m541630517_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t702301140 * _thisAdjusted = reinterpret_cast<Enumerator_t702301140 *>(__this + 1);
	return Enumerator_System_Collections_IEnumerator_get_Current_m541630517(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::Dispose()
extern "C"  void Enumerator_Dispose_m1489800526_gshared (Enumerator_t702301140 * __this, const MethodInfo* method)
{
	{
		__this->set_l_0((List_1_t682628370 *)NULL);
		return;
	}
}
extern "C"  void Enumerator_Dispose_m1489800526_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t702301140 * _thisAdjusted = reinterpret_cast<Enumerator_t702301140 *>(__this + 1);
	Enumerator_Dispose_m1489800526(_thisAdjusted, method);
}
// System.Void System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::VerifyState()
extern Il2CppClass* ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var;
extern Il2CppClass* InvalidOperationException_t1589641621_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3238255034;
extern const uint32_t Enumerator_VerifyState_m1128068487_MetadataUsageId;
extern "C"  void Enumerator_VerifyState_m1128068487_gshared (Enumerator_t702301140 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Enumerator_VerifyState_m1128068487_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t682628370 * L_0 = (List_1_t682628370 *)__this->get_l_0();
		if (L_0)
		{
			goto IL_0026;
		}
	}
	{
		Enumerator_t702301140  L_1 = (*(Enumerator_t702301140 *)__this);
		Il2CppObject * L_2 = Box(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->declaring_type)->rgctx_data, 2), &L_1);
		NullCheck((Il2CppObject *)L_2);
		Type_t * L_3 = Object_GetType_m2022236990((Il2CppObject *)L_2, /*hidden argument*/NULL);
		NullCheck((Type_t *)L_3);
		String_t* L_4 = VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, (Type_t *)L_3);
		ObjectDisposedException_t1794727681 * L_5 = (ObjectDisposedException_t1794727681 *)il2cpp_codegen_object_new(ObjectDisposedException_t1794727681_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m1180707260(L_5, (String_t*)L_4, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_5);
	}

IL_0026:
	{
		int32_t L_6 = (int32_t)__this->get_ver_2();
		List_1_t682628370 * L_7 = (List_1_t682628370 *)__this->get_l_0();
		NullCheck(L_7);
		int32_t L_8 = (int32_t)L_7->get__version_3();
		if ((((int32_t)L_6) == ((int32_t)L_8)))
		{
			goto IL_0047;
		}
	}
	{
		InvalidOperationException_t1589641621 * L_9 = (InvalidOperationException_t1589641621 *)il2cpp_codegen_object_new(InvalidOperationException_t1589641621_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m1485483280(L_9, (String_t*)_stringLiteral3238255034, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_9);
	}

IL_0047:
	{
		return;
	}
}
extern "C"  void Enumerator_VerifyState_m1128068487_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t702301140 * _thisAdjusted = reinterpret_cast<Enumerator_t702301140 *>(__this + 1);
	Enumerator_VerifyState_m1128068487(_thisAdjusted, method);
}
// System.Boolean System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::MoveNext()
extern "C"  bool Enumerator_MoveNext_m530730869_gshared (Enumerator_t702301140 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		((  void (*) (Il2CppObject *, const MethodInfo*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0)->methodPointer)(il2cpp_codegen_fake_box((Enumerator_t702301140 *)__this), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->declaring_type)->rgctx_data, 0));
		int32_t L_0 = (int32_t)__this->get_next_1();
		if ((((int32_t)L_0) >= ((int32_t)0)))
		{
			goto IL_0014;
		}
	}
	{
		return (bool)0;
	}

IL_0014:
	{
		int32_t L_1 = (int32_t)__this->get_next_1();
		List_1_t682628370 * L_2 = (List_1_t682628370 *)__this->get_l_0();
		NullCheck(L_2);
		int32_t L_3 = (int32_t)L_2->get__size_2();
		if ((((int32_t)L_1) >= ((int32_t)L_3)))
		{
			goto IL_0053;
		}
	}
	{
		List_1_t682628370 * L_4 = (List_1_t682628370 *)__this->get_l_0();
		NullCheck(L_4);
		TargetSearchResultU5BU5D_t1920244343* L_5 = (TargetSearchResultU5BU5D_t1920244343*)L_4->get__items_1();
		int32_t L_6 = (int32_t)__this->get_next_1();
		int32_t L_7 = (int32_t)L_6;
		V_0 = (int32_t)L_7;
		__this->set_next_1(((int32_t)((int32_t)L_7+(int32_t)1)));
		int32_t L_8 = V_0;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, L_8);
		int32_t L_9 = L_8;
		TargetSearchResult_t3609410114  L_10 = (L_5)->GetAt(static_cast<il2cpp_array_size_t>(L_9));
		__this->set_current_3(L_10);
		return (bool)1;
	}

IL_0053:
	{
		__this->set_next_1((-1));
		return (bool)0;
	}
}
extern "C"  bool Enumerator_MoveNext_m530730869_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t702301140 * _thisAdjusted = reinterpret_cast<Enumerator_t702301140 *>(__this + 1);
	return Enumerator_MoveNext_m530730869(_thisAdjusted, method);
}
// T System.Collections.Generic.List`1/Enumerator<Vuforia.TargetFinder/TargetSearchResult>::get_Current()
extern "C"  TargetSearchResult_t3609410114  Enumerator_get_Current_m3724049598_gshared (Enumerator_t702301140 * __this, const MethodInfo* method)
{
	{
		TargetSearchResult_t3609410114  L_0 = (TargetSearchResult_t3609410114 )__this->get_current_3();
		return L_0;
	}
}
extern "C"  TargetSearchResult_t3609410114  Enumerator_get_Current_m3724049598_AdjustorThunk (Il2CppObject * __this, const MethodInfo* method)
{
	Enumerator_t702301140 * _thisAdjusted = reinterpret_cast<Enumerator_t702301140 *>(__this + 1);
	return Enumerator_get_Current_m3724049598(_thisAdjusted, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
